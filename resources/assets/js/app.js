require('./bootstrap');

import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import {routes} from './routes';
import modules from './store';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import locale from "element-ui/lib/locale/lang/en";
import {initialize} from './general';
import './assets/main.scss';


Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(ElementUI, { locale });
import "./assets/index.css";

export const router = new VueRouter({
  routes,
  //mode: 'history'
});

export const store = new Vuex.Store({
  ...modules
});

initialize(store, router);

const app = new Vue({
  el: '#app',
  router,
  store
});
