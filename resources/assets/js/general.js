export function initialize(store, router) {
  router.beforeEach((to, from, next) => {
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
    const token = store.state.auth.token;
    const user = store.state.auth.user;


    if(requiresAuth && !token) {
      next('/login');
    } else if(to.path == '/login' && token) {
      next('/partners');
    } else if(to.path == '/add/partners' && !user.addpartner) {
      next('/partners');
    }
    else if(to.meta.isPartnerEdit && !user.editpartner) {
      next('/partners');
    }
    else {
      next();
    }
  });

  axios.interceptors.response.use(null, (error) => {
    if (error.response.status == 401) {
      store.commit('auth/logout');
      router.push('/login');
    }

    return Promise.reject(error);
  });
}