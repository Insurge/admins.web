
export default {
  data() {
    return {
      paginateInfo: {
        pageSize: 100,
        currentPage: 1
      },
      sortInfo: {
				currentColumn: '',
				order: ''
			},
    }
  },
  computed: {
    resultData() {
			let resultData = []
			resultData     = _.chunk(this.filteredData, this.paginateInfo.pageSize)
      let data       =  resultData[this.paginateInfo.currentPage - 1]

      return data
		},
    totalPages() {
      return this.data.length;
    },
    pageSize() {
			return parseInt(this.paginateInfo.pageSize)
    },
    filteredData() {

			let resultData  = this.data ? this.data : []
			let resultOrder = this.sortInfo.order == 'ascending' ? 'asc' : 'desc'
			if (this.sortInfo.currentColumn !== '') {
        
        let data       = this.data
        let columnName = this.sortInfo.currentColumn

        resultData = data.sort((a, b) => {
          let newOrder = resultOrder === 'asc' ? 1 : -1

          if (a[columnName] > b[columnName]) {
            return 1 * newOrder
          } else if (a[columnName] < b[columnName]) {
            return -1 * newOrder
          } else {
            return 0
          }
        })
			}

      return resultData
		},
  },
  methods: {
    setCurrentPage(curPage) {
      this.paginateInfo.currentPage = curPage
    },
    sortChange({ prop, order }) {
			this.sortInfo.currentColumn = prop
			this.sortInfo.order         = order
		},
    getStatus: function (value, column) {

      let style = ''
      if ('disabled' === column) {
        value = value === 0 ? 'Active' : 'Disabled'
        style = value.toLowerCase()
      }

      return {
        value,
        style
      }
    }
  },
}