import Main from '../Main';
import PartnersList from '../pages/Partners'
import PartnerSingle from '../pages/Partner'
import PartnerAdd from '../pages/PartnerAdd'
import PartnerEdit from '../pages/PartnerEdit'
import AdminsList from '../pages/Admins'
import Admin from '../pages/Admin'
import Logs from '../pages/logs/Index'
import PartnerTypes from '../pages/settings/PartnerTypes'
import Contracts from '../pages/settings/Contracts'
import Contract from '../pages/settings/Contract'
import PartnerType from '../pages/settings/PartnerType'
import Auth from '../pages/Auth.vue'
import Sellers from '../pages/Sellers'
import SellerAdd from '../pages/SellerAdd'
import Tickets from '../pages/Tickets'

export const routes = [
    {
        path: '/',
        component: Main,
        children: [
            {
                name: 'Partners',
                path: '/partners',
                component: PartnersList
            },
            {
                name: 'Sellers',
                path: '/sellers',
                component: Sellers
            },
            {
                name: 'Tickets',
                path: '/',
                component: Tickets
            },
            {
                path: '/add/sellers',
                component: SellerAdd
            },
            {
                path: '/edit/seller/:id',
                component: SellerAdd
            },
            {
                path: '/partners/:id',
                component: PartnerSingle,
            },
            {
                path: '/add/partners',
                component: PartnerAdd
            },
            {
                path: '/edit/partner/:id',
                component: PartnerEdit,
                meta: {
                isPartnerEdit: true
                }
            },
            {
                path: '/admins',
                component: AdminsList
            },
            {
                path: '/admins/add/',
                component: Admin
            },
            {
                path: '/admins/edit/:id',
                component: Admin
            },
            {
                path: '/logs',
                component: Logs
            },
            {
                path: '/ptypes',
                component: PartnerTypes
            },
            {
                path: '/ptypes/:id',
                component: PartnerType
            },
            {
                path: '/contracts',
                component: Contracts
            },
            {
                path: '/contracts/add/',
                component: Contract
            },
            {
                path: '/contracts/edit/:id',
                component: Contract
            },
        ],
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/login',
        component: Auth
    }
];
