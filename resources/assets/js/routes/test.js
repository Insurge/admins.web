import Main from '../Main';
import PartnersList from '../pages/Partners'

let opts = {
    routes: [
        {
            path: '/',
            component: Main,
            children: [
                {
                    path: "/partners",
                    name: "partners",
                    component: PartnersList,
                    meta: {
                        permissions: [
                            {
                                role: "",
                                access: false,
                                redirect: "404"
                            }
                        ]
                    }
                }
            ]
        }
    ]
};