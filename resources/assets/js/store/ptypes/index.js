import axios from 'axios'
import { router } from '../../app'

export default {
  namespaced: true,
  state: {
    data: [],
    current: {}
  },
  actions: {
    initData({ commit, state }) {
      return new Promise((resolve, reject) => {
        axios({
          url: 'api/types',
          method: 'GET'
        }).then((response) => {
          commit('setData', response.data.types)
          resolve(state.data)
        }).catch((err) => {
          reject(err)
        })
      })
    },
    initCurrent({ commit }) {
      return new Promise((resolve, reject) => {
        axios({
          url: `api/type/${router.currentRoute.params.id}`,
          method: 'GET'
        }).then((response) => {
          commit('setCurrent', response.data.type)
        })
      })
    },
    update({ state, commit }, {id, data}) {

      return new Promise((resolve, reject) => {
      
        const url = '/api/type/' + id

        axios.put(url, data)
        .then(resp => {
          resolve(resp)
        })
        .catch(error => {
          reject(error.response)
        })
      }) 
      .then(
        resp => {
          commit('setCurrent', resp.data.type)
          commit('setRoute', '/ptypes')
        },
        err => {
          
        },
      )
    },
  },
  mutations: {
    setData(state, data) {
      state.data = data
    },
    setCurrent(state, data) {
      state.current = data
    },
    setRoute(state, data) {
      router.push(data)
    }
  },
  getters: {
    getData(state) {
      return state.data
    },
    getCurrent(state) {
      return state.current
    }
  }
}