import { router } from '../../app';

export default {
  namespaced: true,
  state: {
    data: [],
    ptypes: [],
    current: [],
    errors: []
  },    
  actions: {
    initData({ commit }) {
      return new Promise((resolve) => {
        axios({
          url: '/api/contract',
          method: 'GET',
        }).then(
          resp => {
            let data   = (resp.data.contracts) ? resp.data.contracts : []
            let ptypes = (resp.data.ptypes) ? resp.data.ptypes : []
            commit('setData', data)
            commit('setPtypes', ptypes)
            resolve(true)
          },
          err => {
            reject(err)
          }
        )
      })
    },
    add({ state, commit }, rowData) {

      return new Promise((resolve, reject) => {
        axios({
          url: '/api/contract',
          data: rowData,
          method: 'POST',
        })
        .then(resp => {
          resolve(resp)
        })
        .catch(error => {
          reject(error.response)
        })
      })
      .then(
        resp => {
          commit('setErrors', [])
          commit('setRoute', '/contracts')
        },
        err => {
          let data = (err.data) ? err.data : []
          commit('setErrors', data)
        },
      )
    },
    update({ state, commit }, {id, data}) {

      return new Promise((resolve, reject) => {
      
        const url = '/api/contract/' + id

        axios.put(url, data)
        .then(resp => {
          resolve(resp)
        })
        .catch(error => {
          reject(error.response)
        })
      }) 
      .then(
        resp => {
          commit('setErrors', [])
          commit('setRoute', '/contracts')
        },
        err => {
          let data = (err.data) ? err.data : []
          commit('setErrors', data)
        },
      )
    },
    generate({ state, commit }, {partnerId, type}) {

      return new Promise((resolve, reject) => {
        axios({
          url: '/api/contract/generate',
          data: {
            partnerId,
            type
          },
          method: 'POST',
        })
        .then(resp => {
          resolve(resp)
        })
        .catch(error => {
          reject(error.response)
        })
      })
      .then(
        resp => {
          return resp
        },
        err => {
          return err
        },
      )
    },
    initCurrent({ commit }) {

      const id  = parseInt(router.currentRoute.params.id)
      const url = '/api/contract/' + id

      return new Promise((resolve) => {
        axios({
          url,
          method: 'GET',
        }).then(resp => {
          commit('setCurrent', resp.data.contract ? resp.data.contract : {})
          resolve(true)
        }).catch(err => {
          reject(err)
        })
      })
    },
  },
  mutations: {
    setData(state, data) {
      state.data = data
    },
    setPtypes(state, data) {
      state.ptypes = data
    },
    setCurrent(state, data) {
      state.current = data
    },
    setErrors(state, data) {
      state.errors = data
    },
    setRoute(state, data) {
      router.push(data)
    }
  },
  getters: {
    getData(state) {
      return state.data
    },
    getPtypes(state) {
      return state.ptypes
    },
    getCurrentData(state) {
      return state.current
    },
    getErrors(state) {
      return state.errors
    }
  }
}