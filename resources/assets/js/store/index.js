import partners from './partners';
import admins from './admins';
import auth from './auth';
import ptypes from './ptypes';
import documents from './documents';
import logs from './logs';
import menu from './menu';
import contracts from './contracts';
import sellers   from './sellers';
import tickets from './tickets';

export default {
  modules: {
    partners,
    admins,
    auth,
    ptypes,
    documents,
    logs,
    menu,
    contracts,
    sellers,
    tickets
  },
}