import { router } from '../../app';

export default {
  namespaced: true,
  state: {
    data: [],
    current: [],
    errors: []
  },    
  actions: {
    initData({ commit }) {
      return new Promise((resolve) => {
        axios({
          url: '/api/users',
          method: 'GET',
        }).then(
          resp => {
            let data = (resp.data.users) ? resp.data.users : []
            commit('setData', data)
            resolve(true)
          },
          err => {
            reject(err)
          }
        )
      })
    },
    add({ state, commit }, rowData) {

      return new Promise((resolve, reject) => {
        axios({
          url: '/api/register',
          data: rowData,
          method: 'POST',
        })
        .then(resp => {
          resolve(resp)
        })
        .catch(error => {
          reject(error.response)
        })
      })
      .then(
        resp => {
          commit('setErrors', [])
          commit('setRoute', '/admins')
        },
        err => {
          let data = (err.data) ? err.data : []
          commit('setErrors', data)
        },
      )
    },
    update({ state, commit }, {id, data}) {

      return new Promise((resolve, reject) => {
      
        const url = '/api/user/' + id

        axios.put(url, data)
        .then(resp => {
          resolve(resp)
        })
        .catch(error => {
          reject(error.response)
        })
      }) 
      .then(
        resp => {
          commit('setErrors', [])
          commit('setRoute', '/admins')
        },
        err => {
          let data = (err.data) ? err.data : []
          commit('setErrors', data)
        },
      )
    },
    initCurrent({ commit }) {

      const id  = parseInt(router.currentRoute.params.id)
      const url = '/api/user/' + id

      return new Promise((resolve) => {
        axios({
          url,
          method: 'GET',
        }).then(resp => {
          commit('setCurrent', resp.data.user ? resp.data.user : {})
          resolve(true)
        }).catch(err => {
          reject(err)
        })
      })
    },
  },
  mutations: {
    setData(state, data) {
      state.data = data
    },
    setCurrent(state, data) {
      state.current = data
    },
    setErrors(state, data) {
      state.errors = data
    },
    setRoute(state, data) {
      router.push(data)
    }
  },
  getters: {
    getData(state) {
      return state.data
    },
    getDataByFilter(state) {
      return (filter) => {

        if (filter === 'all') {
          let active = state.data.filter((item) => {
            return item.disabled === 0;
          })
          let disabled = state.data.filter((item) => {
            return item.disabled === 1;
          })
          return active.concat(disabled)
        }
        return state.data.filter((item) => {
          return item.disabled === filter;
        })
      }
    },
    getCurrentData(state) {
      return state.current
    },
    getErrors(state) {
      return state.errors
    },
    getSubmitStatus(state) {
      return state.submit
    }
  }
}