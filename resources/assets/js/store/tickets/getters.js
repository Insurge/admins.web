export default {
    getUnprocessedTickets(state) {
        return state.unprocessed;
    },
    getProcessedTicket(state) {
        return state.processed;
    }
}