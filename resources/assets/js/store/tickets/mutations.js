export default {
    setTickets(state, { unprocessed, processed }) {

        unprocessed.forEach((ticket) => {
            let findedUnprocTicket = state.unprocessed.find(unprocTicket => {
                return unprocTicket.id === ticket.id;
            });

            if (typeof findedUnprocTicket === "undefined") {
                state.unprocessed.push(ticket);
            }

        });
        state.unprocessed = deleteRemovedItems(unprocessed, state.unprocessed, 'unprocessed')

        processed.forEach((ticket) => {
            let findedProcTicket = state.processed.find(procTicket => {
               return procTicket.id === ticket.id;
            });

            if (typeof findedProcTicket === "undefined") {
                state.processed.push(ticket);
            }

        });
        state.processed = deleteRemovedItems(processed, state.processed, 'processed');

    },

    transferToProcessed(state, ticketId) {

        let transferTicket = state.unprocessed.find((ticket) => {
            return ticket.id === ticketId;
        });

        let transferIndex = state.unprocessed.findIndex((ticket) => {
            return ticket.id === ticketId;
        });
        state.unprocessed.splice(transferIndex, 1);

        state.processed.push(transferTicket);

        state.unprocessed.sort(compare);
    },

    transferToUnprocessed(state, ticketId) {
        let transferTicket = state.processed.find((ticket) => {
            return ticket.id === ticketId;
        });

        let transferIndex = state.processed.findIndex((ticket) => {
            return ticket.id === ticketId;
        });
        state.processed.splice(transferIndex, 1);
        state.unprocessed.push(transferTicket);

        state.unprocessed.sort(compare);
    },

    removeFromProcess(state, ticketId) {
        let removeIndex = state.processed.findIndex((ticket) => {
            return ticket.id === ticketId;
        });
        state.processed.splice(removeIndex, 1);
    }
}

function deleteRemovedItems(serverTickets, localTickets, type) {
    let resultTickets = [];
    localTickets.forEach(localTicket => {
        let foundItem = serverTickets.find(serverTicket => {
            return localTicket.id === serverTicket.id;
        });

        if (typeof foundItem !== 'undefined') {
            resultTickets.push(foundItem);
        }
    });
    return resultTickets;
}

function compare(a, b) {
    if (a.date < b.date)
        return -1;
    if (a.date > b.date)
        return 1;
    return 0;
}