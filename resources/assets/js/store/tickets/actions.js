import axios from 'axios';
import { Notification } from 'element-ui';

export default {
    initData({ commit }) {
        return new Promise((resolve) => {
            axios.get('api/tickets')
                .then((response) => {
                    let unprocessed = response.data.unprocessed;
                    let processed = response.data.processed;

                    commit('setTickets', { unprocessed, processed });

                    resolve(unprocessed);
                });
        });
    },

    verify({ commit }, ticketInfo) {

        return new Promise((resolve, reject) => {
            axios.post(`api/tickets/resolve/${ticketInfo.ticketId}`)
                .then((resp) => {
                    commit('transferToProcessed', ticketInfo.ticketId);
                    resolve(resp);
                })
                .catch(({response}) => {
                    if (response.data.hasOwnProperty('message')) {
                        errorNotify(response.data.message);
                    }
                    reject(response)
                })
        })
    },

    reject({ commit }, ticketInfo) {
        return new Promise((resolve, reject) => {
            axios.post(`api/tickets/reject/${ticketInfo.ticketId}/${ticketInfo.reason}`)
                .then((resp) => {
                    commit('transferToProcessed', ticketInfo.ticketId);
                    resolve(resp);
                })
                .catch(({response}) => {
                    if (response.data.hasOwnProperty('message')) {
                        errorNotify(response.data.message);
                    }
                    reject(response)
                })
        })
    },

    cancel({ commit }, ticketInfo) {
        return new Promise((resolve, reject) => {
            axios.post(`api/tickets/cancel/${ticketInfo.ticketId}`)
                .then((resp) => {
                    commit('transferToUnprocessed', ticketInfo.ticketId);
                    resolve(resp);
                })
                .catch(({response}) => {
                    commit('removeFromProcess', ticketInfo.ticketId);

                    if (response.data.hasOwnProperty('message')) {
                        errorNotify(response.data.message);
                    }
                    reject(response);
                })
        })
    }
}

function errorNotify(message) {
    Notification.error({
        title: 'Error',
        message
    })
}