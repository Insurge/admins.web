export default {
    getDataByFilter(state) {
        return (filter, isDisable, column, filterStr) => {
            let filterData = state.data.filter((item) => {
                let value = Array.isArray(column) ? item[column[0]][column[1]] : item[column]
                let filteredVal = false

                let result = {
                    filteredVal:    filteredVal || filter === 'all',
                    disabled:       (item.disabled === isDisable) || isDisable === 'all',
                    filterByColumn: (column !== '' && (value && value.toLowerCase().indexOf(filterStr.toLowerCase()) != -1) || filterStr === ''),
                }
                if (column === '' && filterStr !== '' && result.filteredVal && result.disabled && !result.filterByColumn) {
                    for (let columnKey in state.displaySelesColums) {
                        let value = item[state.displaySelesColums[columnKey].key] || '';
                        if (value.toLowerCase().indexOf(filterStr.toLowerCase()) !== -1) {
                            return true
                        }
                    };
                    return false;
                }
                return result.filteredVal && result.disabled && result.filterByColumn;;
            });
            return filterData.sort((a, b) => {
                if (!a.disabled || b.disabled) {
                    return -1;
                } else if(a.disabled || !b.disabled) {
                    return 1;
                } else {
                    return 0;
                }
            });
        }
    },
    getData(state) {
        return state.data
    },
    getColumns(state) {
        return state.displaySelesColums
    },
    getErrors(state) {
        return state.errors;
    }
}