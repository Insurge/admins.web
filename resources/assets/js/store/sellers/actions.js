import axios from 'axios';

export default {
    save({commit}, {data, action, id}) {
        let resultId = id || '';
        let query = action === 'update' ? `/api/sellers/${action}/${resultId}` : `/api/sellers/${action}`;
        return new Promise((resolve, reject) => {
            let method = action === 'update' ? 'put' : 'post';
            axios[method](query, data)
                .then((response) => {
                    commit('setRoute', '/sellers');
                    commit('setErrors', []);
                    resolve(response.data);
                })
                .catch(err => {
                    commit('setErrors', err.response.data);
                    reject(err);
                });
        }); 
    },
    initData({ commit }) {
        return new Promise((resolve) => {
            axios.get('api/sellers')
                .then((response) => {
                    let sellers = response.data.sellers;
                    commit('setPartners', sellers);
                    resolve(sellers);
                });
        });
    },
    getCurrent({}, id) {
        return new Promise((resolve, reject) => {
            axios.get(`api/sellers/${id}`)
                .then(response => {
                    resolve(response.data.seller)
                })
                .catch(err => {
                    reject(err);
                }) 
        })
    }
}