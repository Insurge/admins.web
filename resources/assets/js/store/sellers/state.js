export default {
    data: [],
    errors: [],
    displaySelesColums: {
        full_name: {
            key: 'name',
            label: 'Full name'
        },
        email: {
            key: 'login',
            label: 'Email'
        },
        phone: {
            key: 'phone',
            label: 'Phone'
        },
        status: {
            key: 'disabled',
            label: 'Status'
        }
    },
}