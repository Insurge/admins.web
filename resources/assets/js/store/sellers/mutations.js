import { router } from '../../app';

export default {
    setPartners(state, sellers) {
        state.data = sellers;
    },
    setRoute(state, data) {
        router.push(data)
    },
    setErrors(state, errors) {
        state.errors = errors
    }
}