import { router } from '../../app';
import axios from 'axios';

const bufToken = localStorage.getItem('user-token');

if (bufToken) {
    axios.defaults.headers.common.Authorization = `Bearer ${bufToken}`;
}

export default {
    namespaced: true,
    state: {
        data: [],
        ptypes: [],
        current: {},
        errors: [],
        displaySelesColums: {
            full_name: {
                key: 'name',
                label: 'Full name'
            },
            email: {
                key: 'email',
                label: 'Email'
            },
            phone: {
                key: 'phone',
                label: 'Phone'
            },
            status: {
                key: 'disabled',
                label: 'Status'
            }
        },
        displayPartnerColumns: {
            partner_name: {
                key: 'partner_name',
                label: 'Name'
            },
            phone: {
                key: 'phone',
                label: 'Phone'
            }
        },
        displayUserColumns: {
            name: {
                key: 'name',
                label: 'Manager'
            },
        },
        payments: []
    },
    actions: {
        initData({ commit }, id) {
            return new Promise((resolve) => {
                axios({ url: 'api/partners', method: 'GET' })
                    .then((response) => {

                        let partners = response.data.partners;
                        if (id) {
                            partners = response.data.partners.filter((item) => {
                                return item ? item.id != id : false;
                            });
                        }
                        commit('setPtypes', response.data.ptypes);
                        commit('setPartners', partners);
                    })
                resolve(true)
            })
        },
        add({ state, commit }, partner) {

            return new Promise((resolve, reject) => {

                axios.post('/api/partners/add', partner)
                    .then((resp) => {
                        const partners = state.data;
                        commit('setRoute', '/partners');
                        commit('setPartners', partners);
                        commit('setErrors', []);
                        resolve(resp);
                    })
                    .catch((err) => {
                        commit('setErrors', err.response.data);
                        reject(err.response)
                    })
            })
        },
        initCurrent({ commit }) {

            return new Promise((resolve) => {
                axios({ url: `api/partners/${router.currentRoute.params.id}`, method: 'GET' })
                    .then((response) => {
                        let partner = response.data.partner;
                        partner.audits = response.data.audits
                        if (!partner) {
                            router.push('/404')
                        }
                        let payments = {out: [], in: []};

                        partner.qrcs.forEach(qrc => {
                            qrc.payments.forEach(payment => {
                                payment.qrc = qrc.code;
                                payments[payment.direction].push(payment);
                            });
                        });

                        commit('setCurrent', partner);
                        commit('setPtypes', partner.ptypes);
                        commit('setPayments', payments);

                        resolve(response.data.partner);
                    })
            })
        },
        save({ commit }, data) {

            const id = parseInt(router.currentRoute.params.id)
            return new Promise((resolve, reject) => {

                axios({ url: '/api/partners/' + id, data, method: 'PUT' })
                    .then((resp) => {
                        commit('setErrors', [])
                        resolve(resp);
                    })
                    .catch((err) => {
                        let data = (err.response.data) ? err.response.data : []
                        commit('setErrors', data)
                        reject(err.data)
                    })
            });
        },

    },
    mutations: {
        setPartners(state, partners) {
            state.data = partners
        },
        setPtypes(state, ptypes) {
            state.ptypes = ptypes
        },
        setCurrent(state, partner) {
            state.current = partner
        },
        setErrors(state, data) {
            state.errors = data
        },
        setRoute(state, data) {
            router.push(data)
        },
        setPayments(state, payments) {
            state.payments = payments;
        },
        addDocument(state, document) {
            state.current.documents.push(document);
        }
    },
    getters: {
        getData(state) {
            return state.data
        },
        getPtypes(state) {
            return state.ptypes
        },
        getDataByFilter(state) {
            return (filter, isDisable, column, filterStr) => {
                let filterData = state.data.filter((item) => {
                    let value = Array.isArray(column) ? item[column[0]][column[1]] : item[column]
                    let filteredVal = false
                    if (item.is_sale && filter === 'sale') {
                        filteredVal = true
                    }

                    if (item.qrcs) {
                        item.qrcs.forEach(function(element) {
                            if (element.ptype_id === filter) {
                                filteredVal = true
                            }
                        })
                    }

                    let result = {
                        filteredVal:    filteredVal || filter === 'all',
                        disabled:       (item.disabled === isDisable) || isDisable === 'all',
                        filterByColumn: (column !== '' && (value && value.toLowerCase().indexOf(filterStr.toLowerCase()) != -1) || filterStr === ''),
                    }

                    if (column === '' && filterStr !== '' && result.filteredVal && result.disabled && !result.filterByColumn) {
                        for (let columnKey in state.displayPartnerColumns) {
                            let value = item[state.displayPartnerColumns[columnKey].key] || '';
                            if (value.toLowerCase().indexOf(filterStr.toLowerCase()) !== -1) {
                                return true
                            }
                        };
                        for (let columnKey in state.displayUserColumns) {
                            // console.log({'user': item.user[state.displayUserColumns[columnKey].key]})
                            let value = item.manager[state.displayUserColumns[columnKey].key] || '';
                            if (value.toLowerCase().indexOf(filterStr.toLowerCase()) !== -1) {
                                return true
                            }
                        };
                        return false;
                    }
                    return result.filteredVal && result.disabled && result.filterByColumn;;
                });

                return filterData.sort((a, b) => {
                    if (!a.disabled || b.disabled) {
                        return -1;
                    } else if(a.disabled || !b.disabled) {
                        return 1;
                    } else {
                        return 0;
                    }
                });
            }
        },
        getCurrentPartner(state) {
            return state.current;
        },
        getErrors(state) {
            return state.errors;
        },
        getPayments(state) {
            return (direction) => {
                return state.payments[direction];
            }
        }
    }
}
