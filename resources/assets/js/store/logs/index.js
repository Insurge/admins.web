function getModelsNames(logs) {
    let set = new Set();
    logs.forEach((log) => {
        set.add(log.doing.modelName);
    });
    return set;
}
function getActionsNames(logs) {
    let set = new Set();
    logs.forEach((log) => {
        set.add(log.doing.actionName);
    });
    return set;
}

export default {
    namespaced: true,
    state: {
        logs: [],
        modelsNames: [],
        users: [],
        actionsNames: []
    },
    actions: {
        initData({commit}) {
            return new Promise((resolve, reject) => {
                    axios.get(`/api/logs`)
                        .then(resp => {
                            resp.data.logs.forEach((log) => {
                                log.doing = JSON.parse(log.doing);
                            });
                            let modelsNames = getModelsNames(resp.data.logs);
                            let actionsNames = getActionsNames(resp.data.logs);
                            commit('setModelsNames', modelsNames);
                            commit('setActionsNames', actionsNames);
                            commit('setData', resp.data.logs);
                            commit('setUsers', resp.data.users);
                            resolve(true);
                        })
                        .catch(err => {
                            console.error(err);
                            reject(err);
                        })
                }
            )
        }
    },
    mutations: {
        setData(state, data) {
            state.logs = data
        },
        setUsers(state, users) {
            state.users = users
        },
        setActionsNames(state, actionsNames) {
            state.actionsNames = actionsNames;
        },
        setModelsNames(state, modelsNames) {
            state.modelsNames = modelsNames;
        }
    },
    getters: {
        getData(state) {
            return state.logs
        },
        getUsers(state) {
            return state.users;
        },
        getActionsNames(state) {
            return Array.from(state.actionsNames);
        },
        getModelsNames(state) {
            return Array.from(state.modelsNames);
        },
        getDataByFilters(state) {

            return (filter) => {

                let filteredData = state.logs
                filter.date = (null === filter.date) ? 'all' : filter.date

                if (filter.user !== 'all') {
                    filteredData = filteredData.filter((item) => {
                        return item.passwd_id === filter.user
                    })
                }

                if (filter.action !== 'all') {
                    filteredData = filteredData.filter((item) => {
                        return item.doing.actionName === filter.action
                    })
                }

                if (filter.model !== 'all') {
                    filteredData = filteredData.filter((item) => {
                        return item.doing.modelName === filter.model;
                    })
                }

                if (filter.date !== 'all') {

                    let start = new Date(filter.date[0])
                    let end = new Date(filter.date[1])
                    filteredData = filteredData.filter((item) => {

                        let cur = new Date(item.created_at)
                        if ((cur < end) && (cur > start)) {
                            return true
                        }
                    })
                }

                return filteredData
            }
        },

    }
}