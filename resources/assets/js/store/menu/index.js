const bufIndex = localStorage.getItem('indexRoute');
const bufLabel = localStorage.getItem('labelRoute')

export default {
  namespaced: true,
  state: {
    menuItems: [
      {
        label: 'Managers',
        index: '/sellers',
        permissions: false
      },
      {
        label: 'Partners',
        index: '/partners',
        permissions: false
      },
      {
        label: 'Admins',
        index: '/admins',
        permissions: 'super'
      },
      {
        label: 'Logs',
        index: '/logs',
        permissions: 'logaccess'
      },
      //{
        //label: 'Tickets',
        //index: '/',
        //permissions: 'is_moderator'
      //}
    ],
    subMenus: [
      {
        label: 'Settings',
        permissions: 'super',
        index: '4',
        items:[ 
          {
            label: 'Partners types',
            index: '/ptypes',
          },
          {
            label: 'Contracts',
            index: '/contracts',
          },
        ]
      }
    ],
    activeItem: {
      label: bufLabel || 'Partners',
      index: bufIndex || '/partners'
    },
    plusRoutings: [
      {
        route: '/partners',
        permissions: 'auth/isAddPartner',
        to: '/add/partners'
      },
      {
        route: '/admins',
        permissions: false,
        to: 'admins/add/'
      },
    ]
  },
  getters: {
    getItems(state) {
      return state.menuItems
    },
    getSubitems(state) {
      return state.subMenus
    },
    getCurrent(state) {
      return state.activeItem
    },
    getItemsWithoutAcive(state) {
      return state.menuItems.filter((item) => {
        return item.index !== state.activeItem.index
      })
    },
    showAddButton(state) {
      return (route) => {
        const result = state.plusRoutings.find((item) => {
          return item.route === route
        })
        return result || false
      }
    }
  },
  actions: {
    setCurrent({ state }, index) {
      let menuItem = state.menuItems.find((item) => {
        return item.index === index
      })
      if ( ! menuItem) {
        state.subMenus.forEach((item) => {
          menuItem = item.items.find((item) => {
            return item.index == index
          })
        })
      }
      localStorage.setItem('indexRoute', menuItem.index)
      localStorage.setItem('labelRoute', menuItem.label)
      state.activeItem = menuItem;
    }
  },
  mutations: {}
};