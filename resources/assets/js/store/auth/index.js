import axios from 'axios'

const bufToken = localStorage.getItem('user-token')
const bufUser = JSON.parse(localStorage.getItem('user'))

if (bufToken) {
  axios.defaults.headers.common.Authorization = `Bearer ${bufToken}`
}

export default {
  namespaced: true,
  state: {
    token: bufToken || "",
    user: bufUser || {}
  },
  actions: {
    login({ commit, dispatch }, user) {
    dispatch('auth/logout', null, {root:true});
    return new Promise((resolve, reject) => {
        axios({ url: "/api/login", data: user, method: "POST" })
          .then(resp => {
            const token = resp.data.token
            const user = resp.data.user
            localStorage.setItem("user-token", token)
            localStorage.setItem("user", JSON.stringify(user))
            axios.defaults.headers.common.Authorization = `Bearer ${token}`
            commit("authSuccess", token)
            commit("setUser", user)
            // you have your token, now log in your user :)
            resolve(resp)
          })
          .catch(err => {
            localStorage.removeItem("user-token")
            reject(err)
          });
      });
    },
    logout({ state }) {
      state.token = ""
      localStorage.removeItem("user-token")
    }
  },
  mutations: {
    authSuccess(state, token) {
      state.token = token
    },
    logout(state) {
      localStorage.removeItem("user-token");
      state.token = ''
    },
    setUser(state, user) {
      state.user = user
    }
  },
  getters: {
    isAuthenticated(state) {
      return state.token !== ""
    },
    getUser(state) {
      return state.user
    },
    isSuper(state) {
      return state.user.super
    },
    isAddPartner(state) {
      return state.user.addpartner
    },
    isEditPartner(state) {
      return state.user.editpartner
    },
    isBlockPartner(state) {
      return state.user.blockpartner
    },
    isViewBalance(state) {
      return state.user.viewbalance
    },
    isAddDocs(state) {
      return state.user.adddocs
    },
    isDisableDocs(state) {
      return state.user.disabledocs
    },
    isLogAccess(state) {
      return state.user.logaccess
    },
    isModer(state) {
      return state.user.is_moderator
    }
  }
};