import { router } from '../../app';

export default {
  namespaced: true,
  state: {
    files: [],
    errors: []
  },    
  actions: {
    add({ state }, document) {
        return new Promise((resolve, reject) => {
            let formData = new FormData();
            formData.append('file', document.file);
            formData.append('name', document.name);
            formData.append('date', document.date);
            formData.append('partner_id', document.partner_id);
            axios.post('/api/document/add', formData)
                .then(response => {
                    resolve(response.data.document);
                })
                .catch(err => {
                    reject(err);
                });
        }) 
    },
    edit({ state, commit }, {partnerId, data}) {

      let route = /partners/ + partnerId
      
      let promises = []

      data.forEach(function(file) {

        let promise = new Promise((resolve, reject) => {

          axios({ url: '/api/document/' + file.id, data: file, method: 'PUT' })
            .then(
              (resp) => {
                resolve(resp);
              },
              (err) => {
                reject(err.response)
              }
            )
        })
          .then(
            (resp) => {
              commit('setRoute', '/partners')
            },
            (err) => {
            }
          )

        promises.push(promise)
      })

      return Promise.all(promises).then(values => { 
      })
    },
    download({ state, commit }, documentId) {
      return new Promise((resolve, reject) => {
        axios({
          url: '/api/document/' + documentId,
          data: {
            documentId
          },
          method: 'GET',
        })
          .then(resp => {
            resolve(resp)
          })
          .catch(error => {
            reject(error.response)
          })
      })
        .then(
          resp => {
            return resp
          },
          err => {
            return err
          },
        )
    }
  },
  mutations: {
    setFiles(state, files) {
      state.files = files
    },
    setRoute(state, data) {
      router.push(data)
    }
  },
  getters: {
    getFiles(state) {
      return state.files
    },
  }
}