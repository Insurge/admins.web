<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://js.squareup.com/v2/paymentform"></script>
    <script src="{{ asset('js/payment/sqpaymentform-basic.js') }}"></script>
    <link href="{{ asset('js/payment/sqpaymentform-basic.css') }}" rel="stylesheet">
</head>
<body>

<div id="form-container">
    <div id="sq-ccbox">
        <!--
          Be sure to replace the action attribute of the form with the path of
          the Transaction API charge endpoint URL you want to POST the nonce to
          (for example, "/process-card")
        -->
        <form id="nonce-form" novalidate action="/api/transaction/{{$pay_hash}}" method="post">
            <fieldset>
                <span class="label">Card Number</span>
                <div id="sq-card-number"></div>

                <div class="third">
                    <span class="label">Expiration</span>
                    <div id="sq-expiration-date"></div>
                </div>

                <div class="third">
                    <span class="label">CVV</span>
                    <div id="sq-cvv"></div>
                </div>

                <div class="third">
                    <span class="label">Postal</span>
                    <div id="sq-postal-code"></div>
                </div>
            </fieldset>

            <button id="sq-creditcard" class="button-credit-card" onclick="requestCardNonce(event)">Pay $1.00</button>

            <div id="error"></div>

            <!--
              After a nonce is generated it will be assigned to this hidden input field.
            -->
            <input type="hidden" id="card-nonce" name="nonce">
        </form>
    </div> <!-- end #sq-ccbox -->


</div>
</body>
</html>
