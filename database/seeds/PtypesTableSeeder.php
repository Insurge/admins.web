<?php

use Illuminate\Database\Seeder;

class PtypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      
        DB::table('ptypes')->insert([
            'kind' => 'referal',
            'cashflow' => 'in',
            'percent' => 10,
            'm1' => 1000,
            'm3' => 2000,
            'm6' => 3000,
            'm12' => 4000,
            'm24' => 5000
        ]);
        DB::table('ptypes')->insert([
            'kind' => 'reseller',
            'cashflow' => 'in',
            'percent' => 10,
            'm1' => 1000,
            'm3' => 2000,
            'm6' => 3000,
            'm12' => 4000,
            'm24' => 5000
        ]);
        DB::table('ptypes')->insert([
            'kind' => 'virtual',
            'cashflow' => 'in',
            'percent' => 10,
            'm1' => 1000,
            'm3' => 2000,
            'm6' => 3000,
            'm12' => 4000,
            'm24' => 5000
        ]);
        DB::table('ptypes')->insert([
            'kind' => 'employer',
            'cashflow' => 'in',
            'percent' => 10,
            'm1' => 1000,
            'm3' => 2000,
            'm6' => 3000,
            'm12' => 4000,
            'm24' => 5000
        ]);
    }
}
