<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('passwds')->insert([
            'name' => 'Admin',
            'login' => 'admin@admin.org',
            'password' => bcrypt('admin'),
            'phone' => '9379992',
            'disabled' => false,
            'super' => true,
            'addpartner' => true,
            'editpartner' => true,
            'blockpartner' => true,
            'adddocs' => true,
            'viewbalance' => true,
            'acceptbills' => true,
            'viewbills' => true,
            'disabledocs' => true,
            'logaccess' => true,
        ]);
    }
}
