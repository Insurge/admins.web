<?php

use Illuminate\Database\Seeder;
use App\Partner;

class AuditSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $partner = Partner::get()->first();
    DB::table('audit')->insert(
      [
        'object_id'   => $partner->id,
        'object_type' => 'App\Partner',
        'action'      => 'login',
        'date'        => '2018-10-10 00:00:00',
      ]
    );
    DB::table('audit')->insert(
      [
        'object_id'   => $partner->id,
        'object_type' => 'App\Partner',
        'action' => 'login',
        'date' => '2018-10-11 01:00:00',
      ]
    );
    DB::table('audit')->insert(
      [
        'object_id'   => $partner->id,
        'object_type' => 'App\Partner',
        'action' => 'login',
        'date' => '2018-10-12 02:00:00',
      ]
    );
  }
}
