<?php
use Illuminate\Database\Seeder;

class PartnersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $user = App\User::where('email', '=', 'admin@admin.org')->get()->first();
    DB::table('partners')->insert([
        'name' => 'dfrgdfg',
        'added_by' => $user->id,
        'address' => 'fdgdgfdg',
        'bank' => 'fdsgdfgdfg',
        'email' => 'dfgfd@dsfg.dfbgv"',
        'website' => 'https://dgdf.sdf',
        'paylevel' => '45645',
        'parent_id' => 0,
        'qrc' => 'fhfhfh',
        'salt' => 'secret',
        'private' => 0,
        'disabled' => 0,
        'signer' => 'qqweqweqwe',
        'base' => 'qweqweqweqwe',
        'swift' => 'qweqweqweqwe',
        'mfo' => 'qweqweqweqwe',
        'account' => 'qweqweqweqwe',
        'contact' => 'asdqwe qsdasd'
    ]);
  }
}
