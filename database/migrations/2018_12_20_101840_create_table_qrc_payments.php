<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableQrcPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qrc_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('qrc_id')->unsigned();
            $table->foreign('qrc_id')->references('id')->on('qrcs');
            $table->integer('amount');
            $table->enum('direction', ['out', 'in']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qrc_payments');
    }
}
