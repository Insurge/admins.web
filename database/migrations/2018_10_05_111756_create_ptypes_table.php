<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePtypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ptypes', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('kind', ['sale', 'referal', 'reseller', 'virtual', 'employer', 'vip']);
            $table->enum('cashflow', ['in', 'out']);
            $table->integer('percent');
            $table->integer('m1');
            $table->integer('m3');
            $table->integer('m6');
            $table->integer('m12');
            $table->integer('m24');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners');
        Schema::dropIfExists('ptypes');
    }
}
