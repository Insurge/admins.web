<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('qrc')->nullable();
            $table->string('photo')->nullable();
            $table->boolean('status')->default(0);

            $table->string('companyId')->nullable();
            $table->string('companyTitle')->nullable();
            $table->string('cellphone')->nullable();
            $table->string('email')->nullable();
            $table->boolean('emailVerified')->default(0);
            $table->boolean('phoneVerified')->default(0);
            $table->string('firstName')->nullable();
            $table->string('middleName')->nullable();
            $table->string('lastName')->nullable();
            $table->string('gender')->nullable();
            $table->date('birthDate')->nullable();
            $table->string('region')->nullable();
            $table->text('idPhoto')->nullable();
            $table->text('selfiePhoto')->nullable();
            $table->string('pay_hash')->index();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
