<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePasswdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('passwds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('login')->unique();
            $table->string('password');
            $table->string('phone')->nullable();
            $table->boolean('disabled')->default(0);
            $table->boolean('super')->default(0);
            $table->boolean('addpartner')->default(0);
            $table->boolean('editpartner')->default(0);
            $table->boolean('blockpartner')->default(0);
            $table->boolean('adddocs')->default(0);
            $table->boolean('viewbalance')->default(0);
            $table->boolean('acceptbills')->default(0);
            $table->boolean('viewbills')->default(0);
            $table->boolean('disabledocs')->default(0);
            $table->boolean('logaccess')->default(0);
            $table->boolean('is_moderator')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('passwds');
    }
}
