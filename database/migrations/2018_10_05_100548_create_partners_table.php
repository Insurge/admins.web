<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('added_by')->unsigned()->nullable();
            $table->foreign('added_by')->references('id')->on('passwds');
            $table->integer('parent_id')->unsigned()->default(0);
            $table->text('salt')->nullable();
            $table->boolean('private')->default(0);
            $table->string('name')->nullable();
            $table->string('edrpo')->nullable();
            $table->string('address')->nullable();
            $table->string('signer')->nullable();
            $table->string('base')->nullable();
            $table->string('paylevel')->nullable();
            $table->boolean('disabled')->default(0);
            $table->string('bank')->nullable();
            $table->string('swift')->nullable();
            $table->string('mfo')->nullable();
            $table->string('account')->nullable();
            $table->string('phone')->nullable();
            $table->string('contact')->nullable();
            $table->string('email')->nullable();
            $table->string('website')->nullable();
            $table->string('hash', 200)->nullable();
            $table->string('login');
            $table->string('password');
            $table->boolean('is_sale')->default(0);
            $table->string('type')->default('nat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners');
    }
}
