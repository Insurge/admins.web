<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'PasswdController@authenticate');
Route::post('transaction/{hash}', 'SquareController@transaction');

// Users
Route::post('users/{qrc}', 'UserController@add');
Route::post('users', 'UserController@addWithoutQrc');
Route::put('users/update/{id}', 'UserController@update');

Route::group(['middleware' => ['jwt.verify']], function() {
 
    // Admins
    Route::post('register', 'PasswdController@register');
    Route::get('user', 'PasswdController@getAuthenticatedUser');
    Route::get('user/{id}', 'PasswdController@getUser');
    Route::put('user/{id}', 'PasswdController@updateUser');
    Route::get('users', 'PasswdController@index');

    // Partners
    Route::get('partners', 'PartnerController@index');
    Route::post('partners/add', 'PartnerController@add');
    Route::get('partners/{id}', 'PartnerController@getPartner');
    Route::put('partners/{id}', 'PartnerController@editPartner');

    // Ptypes
    Route::get('types', 'PtypeController@index');
    Route::get('type/{id}', 'PtypeController@getType');
    Route::put('type/{id}', 'PtypeController@update');

    // Logs
    Route::get('logs', 'LogController@index');

    // Docs
    Route::post('document/store', 'DocumentController@store');
    Route::post('document/add', 'DocumentController@add');
    Route::put('document/{id}', 'DocumentController@update');
    Route::get('document/{id}', 'DocumentController@show');

    // Contracts
    Route::resource('contract', 'ContractController');
    Route::post('contract/generate', 'ContractController@generate');

    // Sellers
    Route::get('sellers', 'SellerController@index');
    Route::post('sellers/add', 'SellerController@add');
    Route::put('sellers/update/{id}', 'SellerController@update');
    Route::get('sellers/{id}', 'SellerController@getSeller');

    // Tickets
    Route::get('tickets', 'TicketController@index');
    Route::get('tickets/{id}', 'TicketController@show');
    Route::post('tickets/resolve/{id}', 'TicketController@resolve');
    Route::post('tickets/cancel/{id}', 'TicketController@cancel');
    Route::post('tickets/reject/{id}/{reason}', 'TicketController@reject');

});
