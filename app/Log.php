<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $fillable = [
        'passwd_id', 'doing', 'created_at'
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Passwd', 'passwd_id');
    }

    public function getOldvalueAttribute($value)
    {
        return json_decode($value);
    }

    public function getNewvalueAttribute($value)
    {
        return json_decode($value);
    }
}
