<?php

namespace App\Audit;


class Converter
{

    private $data;

    /**
     * Converter constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return bool
     */
    public function onlyNewValue() : bool {
        $newIsSet = (isset($this->data['newValue']) && !empty($this->data['newValue']));
        $oldIsNotSet = (!isset($this->data['oldValue']) || empty($this->data['oldValue']));
        if ($newIsSet && $oldIsNotSet) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function emptyValues() : bool {
        $newIsNotSet = !isset($this->data['newValue']) || empty($this->data['oldValue']);
        $oldIsNotSet = !isset($this->data['newValue']) || empty($this->data['oldValue']);
        if ($newIsNotSet && $oldIsNotSet) {
            return true;
        }else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function hasObject() : bool {
        return isset($this->data['object']) && !empty($this->data['object']);
    }

    /**
     * @return string
     */
    public function getNewValue() : string {
        return $this->data['newValue'];
    }

    /**
     * @return string
     */
    public function getOldValue() : string {
        return $this->data['oldValue'];
    }

    /**
     * @return string
     */
    public function getModelUsed() : string {
        return $this->data['modelUsed'];
    }

    /**
     * @return array
     */
    public function getObject() : array {
        return $this->data['object'];
    }

    /**
     * @return string
     */
    public function getActionName() : string {
        return $this->data['actionName'];
    }

}