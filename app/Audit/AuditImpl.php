<?php

namespace App\Audit;


class AuditImpl implements AuditInterface
{

    /**
     * @var Converter
     */
    private $data;

    /**
     * AuditImpl constructor.
     * @param string $auditData
     */
    public function __construct(string $auditData)
    {
        $this->data = new Converter(json_decode($auditData, true));
    }


    /**
     * @inheritdoc
     */
    public function generateHtml(): string
    {
        $resultHtml = "";

        if ($this->data->hasObject()) {
            $resultHtml .= $this->generateModelHtmlWithObj();
        } else {
            $resultHtml .= $this->generateModelHtml();
        }

        $resultHtml .= $this->generateActionHtml();
        if ($this->data->onlyNewValue()) {
            info(['generateHtmlOnlyForNew' => $this->generateHtmlOnlyForNew()]);
            $resultHtml .= $this->generateHtmlOnlyForNew();
        } elseif(!$this->data->emptyValues()) {
            $resultHtml .= $this->generateHtmlForAllValues();
        }

        return $resultHtml;
    }


    /**
     * @inheritdoc
     */
    public function generateModelHtml(): string
    {
        return "<p>Object: <b>" . $this->data->getModelUsed() . "</b><p>";
    }

    /**
     * @inheritdoc
     */
    public function generateModelHtmlWithObj(): string
    {
        $dataString = "<p>{";

        foreach ($this->data->getObject() as $key => $value) {
            $dataString .= "<span> $key: </span><b>$value</b>";
        }

        $dataString .= " }</p>";

        return $this->generateModelHtml() . $dataString;
    }


    /**
     *
     * @inheritdoc
     */
    public function generateActionHtml(): string
    {
        return "<p>Action: <b>". $this->data->getActionName() ."</b></p>";
    }

    /**
     * @inheritdoc
     */
    public function generateHtmlOnlyForNew(): string
    {
        return "<p>To: <b>" . $this->data->getNewValue() . "</b></p>";
    }

    /**
     * @inheritdoc
     */
    public function generateHtmlForAllValues() : string {
        return "<p>From: <b>" . $this->data->getOldValue() . "</b> To: <b>" . $this->data->getNewValue() . "</b></p>";
    }

}