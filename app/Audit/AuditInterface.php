<?php

namespace App\Audit;


interface AuditInterface
{
    /**
     * Render the html
     *
     * @return string
     */
    public function generateHtml() : string;

    /**
     * Generate HTML code for model
     *
     * @return string
     */
    public function generateModelHtml() : string;

    /**
     * Generate HTML code for object with model
     *
     * @return string
     */
    public function generateModelHtmlWithObj() : string;

    /**
     * Generate HTML code for action description
     *
     * @return string
     */
    public function generateActionHtml() : string;

    /**
     * Generate HTML for "create" action
     *
     * @return string
     */
    public function generateHtmlOnlyForNew() : string;

    /**
     * Generate HTML fro "update" action
     *
     * @return string
     */
    public function generateHtmlForAllValues() : string;

}