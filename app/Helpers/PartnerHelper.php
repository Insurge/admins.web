<?php

namespace App\Helpers;

use Illuminate\Validation\Rule;
use Validator;
use App\Partner;

class PartnerHelper {

    const ADD_RULES = [
        'name' => 'required|string|max:255',
        'paylevel' => 'required|numeric|min:5000',
        'email' => 'required|string|email|max:255',
        'phone'    => 'required|regex:/[0-9]{11}/',
        'bank' => 'required|string|max:255',
        'swift' => 'required|string|max:255',
        'mfo' => 'required|regex:/[0-9]{6}/',
        'account' => 'required|string|max:255',
        'login' => 'required|unique:partners',
        'parent_id' => 'required|not_in:0'
    ];

    public function validateAdd($properties) {
        $validator = Validator::make($properties, self::ADD_RULES);
        return $validator;
    }

    public function generateObject($properties) {
        $partner = new Partner;
        foreach ($partner->getFillable() as $value) {
            $partner[$value] = $properties[$value];
        }
        return $partner;
    }

    public function generateSalt($min = 0, $max = 0x7FFFFFFF)
    {
        $diff = $max - $min;
        if ($diff < 0 || $diff > 0x7FFFFFFF) {
            throw new \RuntimeException("Bad range");
        }
        $bytes = random_bytes(4);
        if ($bytes === false || strlen($bytes) != 4) {
            throw new \RuntimeException("Unable to get 4 bytes");
        }
        $ary = unpack("Nint", $bytes);
        $val = $ary['int'] & 0x7FFFFFFF;
        $fp = (float) $val / 2147483647.0;

        return round($fp * $diff) + $min;
    }
}