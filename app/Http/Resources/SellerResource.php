<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class SellerResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'login' => $this->login,
            'phone' => $this->phone,
            'disabled' => $this->disabled
        ];
    }
}
