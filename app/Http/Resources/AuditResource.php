<?php

namespace App\Http\Resources;

use App\Audit\AuditImpl;
use Illuminate\Http\Resources\Json\Resource;

class AuditResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $description = new AuditImpl($this->action);

        return [
            'id' => $this->id,
            'ip' => $this->ip,
            'action' => $description->generateHtml(),
            'created_at' => $this->created_at->format('Y-m-d H:i:s')
        ];
    }
}
