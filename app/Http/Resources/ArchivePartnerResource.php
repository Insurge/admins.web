<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ArchivePartnerResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'bank' => $this->bank,
            'disabled' => $this->disabled,
            'edrpo' => $this->edrpo,
            'login' => $this->login,
            'id' => $this->id,
            'partner_name' => $this->name,
            'phone' => $this->phone,
            'qrcs' => $this->qrcs,
            'is_sale' => $this->is_sale,
            'manager' => new ManagerPartnerResource($this->parent),
        ];
    }
}
