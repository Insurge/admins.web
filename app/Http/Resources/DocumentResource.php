<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class DocumentResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $filename = $this->path;

        $file = '../storage/app/' . $filename;
        $file = file_get_contents($file, true);

        $code = chunk_split(base64_encode($file));
        $code = str_replace(array("\r","\n"),"",$code);
        return [
            'id' => $this->id,
            'path' => $code,
            'date' => $this->date,
            'name' => $this->name,
            'created_at' => $this->created_at->format('Y-m-d')
        ];
    }
}
