<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class QrcResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->qrc,
            'ptype_id' => new PTypesResource($this->ptype),
            'disabled' => $this->disabled,
            'name' => $this->name,
            'disabled' => $this->disabled,
            'payments' => QrcPaymentsResource::collection($this->payments)
        ];
    }
}
