<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class PartnerResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'account' => $this->account,
            'address' => $this->address,
            'bank' => $this->bank,
            'base' => $this->base,
            'contact' => $this->contact,
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            'disabled' => $this->disabled,
            'edrpo' => $this->edrpo,
            'email' => $this->email,
            'id' => $this->id,
            'is_sale' => $this->is_sale,
            'mfo' => $this->mfo,
            'type' => $this->type,
            'partner_name' => $this->name,
            'name' => $this->name,
            'paylevel' => $this->paylevel,
            'phone' => $this->phone,
            'signer' => $this->signer,
            'swift' => $this->swift,
            'updated_at' => $this->updated_at->format('Y-m-d H:i:s'),
            'website' => $this->website,
            'manager' => new ArchivePartnerResource($this->parent),
            'qrcs' => QrcResource::collection($this->qrcs),
            'documents' => DocumentResource::collection($this->documents),
            'ptypes' => PTypesResource::collection($this->ptypes)
        ];
    }
}
