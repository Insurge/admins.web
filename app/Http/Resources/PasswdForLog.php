<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class PasswdForLog extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'phone' => $this->phone,
            'password' => $this->password,
            'disabled' => $this->disabled,
            'super' => $this->super,
            'addpartner' => $this->addpartner,
            'editpartner' => $this->editpartner,
            'blockpartner' => $this->blockpartner,
            'adddocs' => $this->adddocs,
            'viewbalance' => $this->viewbalance,
            'acceptbills' => $this->acceptbills,
            'viewbills' => $this->viewbills,
            'disabledocs' => $this->disabledocs,
            'logaccess' => $this->logaccess,
            'is_moderator' => $this->is_moderator
        ];
    }

    /**
     * @param PasswdResource $passwdResource
     * @return array
     */
    public function getDifferentFields(PasswdForLog $passwdResource) : array {

        $arrayResolved = $this->resolve();
        $resultFields = [];
        foreach ($arrayResolved as $key => $value) {
            if ($passwdResource->$key !== $value) {
                $resultFields[$key] = $value;
                if ($key === 'password') {
                    $resultFields[$key] = 'Password was change';
                }
            }
        }

        return $resultFields;
    }
}
