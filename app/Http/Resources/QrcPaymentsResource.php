<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class QrcPaymentsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'amount' => $this->amount,
            'created_at' => $this->created_at->format('Y-m-d'),
            'direction' => $this->direction
        ];
    }
}
