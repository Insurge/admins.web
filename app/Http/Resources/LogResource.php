<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class LogResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'passwd_id' => $this->passwd_id,
            'doing' => $this->doing,
            'oldvalue' => $this->oldvalue,
            'newvalue' => $this->newvalue,
            'object_id' => $this->object_id,
            'object_type' => $this->object_type,
            'created_at' => $this->created_at,
            //'user' => $this->user
        ];
    }
}
