<?php

namespace App\Http\Controllers;
use App\Log\Admin\AddAdmin;
use App\Passwd;
use App\Services\LogService;
use App\Services\PasswdService;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class PasswdController extends Controller
{
    /**
     * All users
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $c_user = JWTAuth::parseToken()->authenticate();
        if (!$c_user->super) {
            return response()->json(['error' => 'don’t_have_permission'], 403);
        }

        $users = Passwd::orderBy('created_at', 'desc')->get();

        return response()->json([
            'users' => $users
        ], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('login', 'password');

        try {
            if ( ! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        $user = JWTAuth::user();
        if ( $user->disabled ){
            return response()->json(['error' => 'access_denied'], 401);
        }

        return response()->json(compact('token', 'user'));
    }

    /**
     * @param Request $request
     * @param PasswdService $passwdService
     * @param LogService $logService
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function register(Request $request, PasswdService $passwdService, LogService $logService)
    {
        $c_user = JWTAuth::parseToken()->authenticate();
        if (!$c_user->super) {
            return response()->json(['error' => 'don’t_have_permission'], 403);
        }

        $user = $passwdService->create($request);
        info(compact('user'));

        return response()->json($user, 201);
    }

    public function getUser($id)
    {

        $c_user = JWTAuth::parseToken()->authenticate();
        if ( ! $c_user->super) {
            return response()->json(['error' => 'don’t_have_permission'], 403);
        }

        $user   = Passwd::whereId($id)->first();

        return response()->json([
            "user" => $user
        ], 200);
    }

    /**
     * @param Request $request
     * @param PasswdService $passwdService
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUser(Request $request, PasswdService $passwdService, $id)
    {
        $c_user = JWTAuth::parseToken()->authenticate();
        if (!$c_user->super) {
            return response()->json(['error' => 'don’t_have_permission'], 403);
        }

        return $passwdService->update($request, $id);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAuthenticatedUser()
    {

        try {
            if ( ! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());
        }

        return response()->json(compact('user'));
    }
}