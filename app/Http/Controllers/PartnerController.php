<?php

namespace App\Http\Controllers;

use App\Http\Resources\AuditResource;
use App\Http\Resources\PartnerResourceForLog;
use App\Audit;
use App\Log\Partner\AddPartner;
use App\Log\Partner\EditPartner;
use App\Partner;
use App\Ptype;
use App\Helpers\PartnerHelper;
use App\Services\LogService;
use App\Services\QRCService;
use App\Http\Resources\PartnerResource;
use App\Http\Resources\ArchivePartnerResource;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Exception;

class PartnerController extends Controller
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $c_user = JWTAuth::parseToken()->authenticate();

        if ( ! $c_user->super) {
            return response()->json(['error' => 'Permission denied'], 400);
        }
        $partners = ArchivePartnerResource::collection(Partner::where('is_sale', 0)->get());
        $ptypes = Ptype::select('id','kind')->get();

        return response()->json(compact('partners', 'ptypes'), 200);
    }

    public function add(Request $request, PartnerHelper $partnerHelper, LogService $logService)
    {
        $validator = $partnerHelper->validateAdd($request->all());
        if ($validator->fails()) {
            info($validator->errors());
            return response()->json($validator->errors(), 400);
        }

        $user = JWTAuth::parseToken()->authenticate();
        $password = str_random(12);
        $hash_password = Hash::make($password);
        $params = $request->all();
        $params['password'] = $hash_password;
        $params['added_by'] = $user->id;
        $params['salt'] = $partnerHelper->generateSalt();

        $partner = $partnerHelper->generateObject($params);

        $partner->save();

        try {
            Mail::send('emails.mail', compact('password'), function($message) use ($partner) {
                $message->to($partner->login, $partner->login)
                    ->subject('Safor');
                $message->from($partner->login,'Safor');
            });
        } catch (Exception $ex) {
            info('bad send message');
        }

        $logService->addLog(new AddPartner($partner->id));

        return response()->json([
            "partner" => $partner
        ], 201);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPartner($id)
    {
        $partner   = new PartnerResource(Partner::find($id));
        $allAudits  =  Audit::where('object_id', '=', $id)->orderBy('created_at', 'desc')->take(10)->get();
        $audits = AuditResource::collection($allAudits);
        return response()->json(compact('partner', 'audits'), 200);
    }

    /**
     * @param Request $request
     * @param $id
     * @param QRCService $qrcService
     * @param LogService $logService
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function editPartner(Request $request, $id, QRCService $qrcService, LogService $logService)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'paylevel' => 'required|numeric|min:5000',
            'bank' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'swift' => 'required|string|max:255',
            'mfo' => 'required|regex:/[0-9]{6}/',
            'account' => 'required|string|max:255',
            'parent_id' => 'required|not_in:0'

        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $partner = Partner::findOrFail($id);

        $logService->addLog(new EditPartner($partner, $request));


        $partner->update($request->all());

        $pTypes = Ptype::all();
        $newQrcs = [];

        foreach($pTypes as $pType) {
            $newQrcs[$pType->kind] = $qrcService->addQRCs($partner, $pType, $request["{$pType->kind}Qrcs"], $logService);
        }
        foreach($request->updatedQrcs as $qrc) {
            $qrcService->updateQrc($qrc['id'], $qrc['name'], $qrc['disabled'], $logService);
        }

        return response()->json([
            "partner" => $partner,
            "savedQrcs" => $newQrcs
        ], 200);
    }
}