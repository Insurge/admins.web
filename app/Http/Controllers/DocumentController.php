<?php

namespace App\Http\Controllers;

use App\Document;
use App\Audit;
use App\Http\Resources\DocumentResource;

use App\Log\Document\AddDocument;
use App\Services\LogService;
use Illuminate\Http\File;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class DocumentController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      //
  }

  public function add(Request $request, LogService $logService)
  {
    $c_user = JWTAuth::parseToken()->authenticate();
    if ( ! $c_user->adddocs) {
      return response()->json(['error' => 'don’t_have_permission'], 403);
    }

    $params = $request->all();
    $file = $request->file('file');
    $name = $request->name . "." . $file->getClientOriginalExtension();
    $date = $request->date;
    $path = $file->storeAs('documents', $name);

    $params['name'] = $name;
    $params['date'] = $date;
    $params['path'] = $path;

    $document = new DocumentResource(Document::create($params));

    $logService->addLog(new AddDocument($document));

    return response()->json(compact('document'), 201);
  }

  public function show($id)
  {
      $document = Document::whereId($id)->first();
      $filename = $document->path;

      $file = '../storage/app/' . $filename;
      $file = file_get_contents($file, true);

      $code = chunk_split(base64_encode($file));
      $code = str_replace(array("\r","\n"),"",$code);
      return response()->json(['doc_name' => $document->name, 'code' => $code], 200);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Document  $document
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $c_user = JWTAuth::parseToken()->authenticate();
    if ( ! $c_user->disabledocs) {
      return response()->json(['error' => 'don’t_have_permission'], 403);
    }

    $document = Document::findOrFail($id);
    $document->update($request->except(['id']));

    return response()->json([
      "document" => $document
    ], 200);
  }
}
