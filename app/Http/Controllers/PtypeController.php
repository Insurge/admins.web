<?php

namespace App\Http\Controllers;

use App\Ptype;
use App\Http\Resources\PTypesResource;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class PtypeController extends Controller
{
    public function index()
    {

      $c_user = JWTAuth::parseToken()->authenticate();
      if ( ! $c_user->super) {
        return response()->json(['error' => 'don’t_have_permission'], 403);
      }
  
      $types = PTypesResource::collection(Ptype::all());

      return response()->json([
        'types' => $types
      ], 200);
    }

    public function getType($id)
    {
      $c_user = JWTAuth::parseToken()->authenticate();
      if ( ! $c_user->super) {
        return response()->json(['error' => 'don’t_have_permission'], 403);
      }

      $type = new PTypesResource(Ptype::whereId($id)->first());

      return response()->json([
        "type" => $type
      ], 200);
    }

    public function update(Request $request, $id) 
    {
      $c_user = JWTAuth::parseToken()->authenticate();
      if ( ! $c_user->super) {
        return response()->json(['error' => 'don’t_have_permission'], 403);
      }
 
      $params = $request->all();
      $type   = Ptype::findOrFail($id);
      $type->update($params);
  
      return response()->json([
        "type" => $type
      ], 200);
    }
}
