<?php

namespace App\Http\Controllers;

use App\Contract;
use App\Ptype;
use App\Partner;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class ContractController extends Controller
{

  public function generate(Request $request) {

    $type       = $request->get('type');
    $partner_id = $request->get('partnerId');
  
    $partner  = Partner::findOrFail($partner_id);
    $contract = Contract::where('ptype', '=', $type)->firstOrFail();
    $date     = new \DateTime();

    $search = [
      '%name%',
      '%edrpo%',
      '%address%',
      '%bank%',
      '%swift%',
      '%mfo%',
      '%account%',
      '%phone%',
      '%website%',
      '%contact%',
      '%email%',
      '%signer%',
      '%base%',
      '%paylevel%',
      '%date%',
    ];
    $replace = [
      $partner->name,
      $partner->edrpo,
      $partner->address,
      $partner->bank,
      $partner->swift,
      $partner->mfo,
      $partner->account,
      $partner->phone,
      $partner->website,
      $partner->contact,
      $partner->email,
      $partner->signer,
      $partner->base,
      $partner->paylevel,
      $date->format('d.m.Y'),
    ];

    $text = $contract->template;
    $text = str_replace($search, $replace, $text);

    $pdf = \App::make('dompdf.wrapper');
    $pdf->loadHTML($text);
    $file = $pdf->stream('contract.pdf');

    return chunk_split(base64_encode($file));
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $c_user = JWTAuth::parseToken()->authenticate();
      if ( ! $c_user->super) {
        return response()->json(['error' => 'don’t_have_permission'], 403);
      }

      $contracts = Contract::orderBy('created_at', 'desc')->get();
      $ptypes    = Ptype::get()->toArray();
      
      return response()->json([
        'contracts' => $contracts,
        'ptypes'    => $ptypes,
      ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
           
      $c_user = JWTAuth::parseToken()->authenticate();
      if ( ! $c_user->super) {
        return response()->json(['error' => 'don’t_have_permission'], 403);
      }

      $contract = Contract::create($request->all());
     
      return response()->json([
        "contract" => $contract
      ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      
      $c_user = JWTAuth::parseToken()->authenticate();
      if ( ! $c_user->super) {
        return response()->json(['error' => 'don’t_have_permission'], 403);
      }

      $contract = Contract::whereId($id)->first();
      $ptypes   = Ptype::get()->toArray();
      $contract['ptypes'] = $ptypes;

      return response()->json([
        "contract" => $contract
      ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
            
      $c_user = JWTAuth::parseToken()->authenticate();
      if ( ! $c_user->super) {
        return response()->json(['error' => 'don’t_have_permission'], 403);
      }

      $contract = Contract::whereId($id)->first();
     
      return response()->json([
        "contract" => $contract
      ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $c_user = JWTAuth::parseToken()->authenticate();
      if ( ! $c_user->super) {
        return response()->json(['error' => 'don’t_have_permission'], 403);
      }

      $contract = Contract::findOrFail($id);
      $contract->update($request->all());
     
      return response()->json([
        "contract" => $contract
      ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
