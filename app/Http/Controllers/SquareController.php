<?php

namespace App\Http\Controllers;
use App\Square\SquareService;
use App\User;
use Illuminate\Http\Request;

class SquareController extends Controller
{
    public function transaction(Request $request, SquareService $squareService, string $hash)
    {
        $user = User::where('pay_hash', $hash)->first();

        if (!$user) {
            return response()->json([
                'error' => 'User not found'
            ]);
        }

        return $squareService->charge([
            'amount' => 500,
            'card_nonce' => 'fake-card-nonce-ok',
            'location_id' => 'CBASEM92yB3AF-LXdF48T5fNHHcgAQ',
            'currency' => 'GBP'
        ], $user->id);
    }
}
