<?php

namespace App\Http\Controllers;

use App\User;

class PaymentController extends Controller
{
    /**
     * @param string $pay_hash
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    function getPaymentForm(string $pay_hash) {
        $user = User::where('pay_hash', $pay_hash)->first();

        if (!$user) {
            return abort(404);
        }
        return view('payment')->with(compact('pay_hash'));
    }
}
