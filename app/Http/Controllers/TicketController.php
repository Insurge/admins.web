<?php

namespace App\Http\Controllers;

use App\Http\Resources\TicketResource;
use App\Services\TicketService;
use App\Ticket;
use Symfony\Component\HttpFoundation\Session\Session;

class TicketController extends Controller
{
    const ERROR_NOT_FOUND_TICKET = 'Ticket was processed';
    const SUCCESS_TICKET_ADD_TO_QUEUE = 'Ticket added to Queue';
    const SUCCESS_TICKET_REMOVED_FROM_QUEUE = 'Ticket remove from Queue';



    public function index()
    {
        $tickets = TicketResource::collection(Ticket::all());
        $unprocessed = [];
        $processed = [];
        foreach ($tickets as $ticket) {
            $session = new Session();
            $value = $session->has('ticket_' . $ticket->id);

            if ($value) {
                $processed[] = $ticket;
            } else {
                $unprocessed[] = $ticket;
            }
        }
        return response()->json(compact('processed', 'unprocessed'), 200);
    }

    public function show($id)
    {
        $ticket = new TicketResource(Ticket::find($id));

        return response()->json([
            'ticket' => $ticket
        ], 200);
    }

    /**
     * @param int $id
     * @param TicketService $ticketService
     * @return \Illuminate\Http\JsonResponse
     */
    public function resolve(int $id, TicketService $ticketService) {
        if (!$ticketService->checkIfExist($id)) {
            return response()->json(['message' => self::ERROR_NOT_FOUND_TICKET], 404);
        }

        $ticketService->resolve($id);

        return response()->json(['message' => 'Ticket added to Queue']);
    }

    /**
     * @param string $reason
     * @param int $id
     * @param TicketService $ticketService
     * @return \Illuminate\Http\JsonResponse
     */
    public function reject(int $id, string $reason, TicketService $ticketService) {
        if (!$ticketService->checkIfExist($id)) {
            return response()->json(['message' => self::ERROR_NOT_FOUND_TICKET], 404);
        }

        $ticketService->reject($id, 'reject', $reason);

        return response()->json(['message' => self::SUCCESS_TICKET_ADD_TO_QUEUE]);
    }

    /**
     * @param int $id
     * @param TicketService $ticketService
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancel(int $id, TicketService $ticketService) {
        if (!$ticketService->checkIfExist($id)) {
            return response()->json(['message' => self::ERROR_NOT_FOUND_TICKET], 404);
        }
        $ticketService->cancel($id);

        return response()->json(['message' => self::SUCCESS_TICKET_REMOVED_FROM_QUEUE]);
    }
}
