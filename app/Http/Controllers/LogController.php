<?php

namespace App\Http\Controllers;

use App\Http\Resources\PasswdResource;
use App\Log;
use App\Passwd;
use Illuminate\Http\Request;

class LogController extends Controller
{
    public function index()
    {

        $logs = Log::all();
        $users = PasswdResource::collection(Passwd::all());


        return response()->json(compact('users', 'logs'), 200);
    }

    public function getLog($id)
    {
        $log = Log::whereId($id)->first();

        return response()->json([
            "log" => $log
        ], 200);
    }

    public function updateLog(Request $request, $id)
    {

    }
}
