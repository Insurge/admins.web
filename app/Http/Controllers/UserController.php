<?php

namespace App\Http\Controllers;

use App\Qrc;
use App\Services\HashService;
use App\Services\SavePhotoService;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request as RequestAlias;
use Illuminate\Support\Str;

class UserController extends Controller
{
    protected $fieldsForUpdate = ['firstName', 'middleName', 'lastName', 'photo'];

    /**
     * @param RequestAlias $request
     * @param SavePhotoService $photoService
     * @param string $hash
     * @param HashService $hashService
     * @return JsonResponse
     */
    public function add(RequestAlias $request, SavePhotoService $photoService, HashService $hashService, string $hash = null)
    {

        $photos = [
            'photo', 'idPhoto', 'selfiePhoto'
        ];

        $data = $request->all();

        $data['qrc'] = $hash;
        $data['pay_hash'] = $hashService->hash(Str::random(10));

        $user = User::create($data);


        $user->tickets()->create();

        $photoService->save($request, $user, $photos);

        return response()->json([
            'user' => $user
        ], 200);
    }

    /**
     * @param RequestAlias $request
     * @param int $id
     * @param SavePhotoService $photoService
     * @return JsonResponse
     */
    public function update(RequestAlias $request, int $id, SavePhotoService $photoService)
    {
        $photos = [
            'photo', 'idPhoto', 'selfiePhoto'
        ];

        $data = $request->all();
        $user = User::find($id);
        $user->firstName = $data['firstName'];
        $user->middleName = $data['middleName'];
        $user->lastName = $data['lastName'];

        $user->save();

        $user->tickets()->create();
        $photoService->save($request, $user, $photos);

        return response()->json([
            'user' => $user
        ]);
    }

    /**
     * @param RequestAlias $request
     * @param SavePhotoService $photoService
     * @param HashService $hashService
     * @return JsonResponse
     */
    public function addWithoutQrc(RequestAlias $request, SavePhotoService $photoService, HashService $hashService) {
        return $this->add($request, $photoService, $hashService);
    }
}
