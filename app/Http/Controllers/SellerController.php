<?php

namespace App\Http\Controllers;

use App\Http\Resources\SellerResource;
use App\Log\Seller\EditSeller;
use App\Partner;
use App\Services\LogService;
use App\Services\SellerService;
use Illuminate\Http\Request;
use JWTAuth;
use Illuminate\Support\Facades\Validator;


class SellerController extends Controller
{
    public function index()
    {
        $current_user = JWTAuth::parseToken()->authenticate();

        if (!$current_user->super) {
            return response()->json(['error' => 'Permission denied'], 400);
        }

        $sellers = Partner::where('is_sale', 1)->get();
        $sellers = SellerResource::collection($sellers);

        return response()->json([
            'sellers' => $sellers
        ], 200);
    }

    public function add(Request $request, SellerService $sellerService)
    {
        $user = JWTAuth::parseToken()->authenticate();

        return $sellerService->create($request, $user->id);
    }

    public function update(Request $request, $id, LogService $logService)
    {
        $validator = Validator::make($request->except(['login']), [
            'name' => 'required|string|max:255',
            'phone' => 'required|regex:/[0-9]{11}/'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $seller = Partner::whereId($id)->first();        

        $oldSellerForLog = new SellerResource($seller->replicate());

        $seller->update($request->all());

        $sellerResource = new SellerResource($seller);

        $logService->addLog(new EditSeller($oldSellerForLog, $sellerResource, $sellerResource->id));

        return response()->json([
            'seller' => $sellerResource
        ], 200);
    }

    public function getSeller($id) {
        
        $seller = Partner::find($id);
        $seller = new SellerResource($seller);

        return response()->json([
            'seller' => $seller
        ], 200);
    }
}
