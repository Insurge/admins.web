<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QrcPayments extends Model
{
    protected $table = 'qrc_payments';
}
