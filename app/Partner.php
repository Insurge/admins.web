<?php

namespace App;

use App\Log\LogFields;
use Illuminate\Database\Eloquent\Model;

class Partner extends Model implements LogFields
{
    protected $fillable = [
        'added_by', 'parent_id', 'salt', 'private', 'name',
        'edrpo', 'address', 'signer', 'base', 'paylevel',
        'disabled', 'bank', 'swift', 'mfo', 'account',
        'phone', 'contact', 'email', 'website', 'is_sale',
        'login', 'password', 'type'
    ];

    /**
     * Get children partners
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany('App\Partner', 'parent_id', 'id');
    }

    /**
     * Get parent partners
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function parent()
    {
        return $this->hasOne('App\Partner', 'id', 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function documents()
    {
        return $this->hasMany('App\Document');
    }

    /**
     * Many to many relationship
     */
    public function ptypes() {
        return $this->belongsToMany(Ptype::class, 'qrcs', 'partner_id', 'ptype_id')
            ->groupBy('qrcs.partner_id', 'qrcs.ptype_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manager()
    {
        return $this->belongsTo('App\Passwd', 'added_by', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logs()
    {
        return $this->hasMany('App\Log');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function qrcs()
    {
        return $this->hasMany('App\Qrc');
    }

    public function getFillable() {
        return $this->fillable;
    }

    public function invite()
    {
        return $this->hasOne('App\Invite');
    }

    public function getFieldForLog() : array {
        return [
            'name' => $this->name,
            'address' => $this->address,
            'bank' => $this->bank,
            'base' => $this->base,
            'contact' => $this->contact,
            'disabled' => $this->disabled,
            'type' => $this->type,
            'edrpo' => $this->edrpo,
            'email' => $this->email,
            'mfo' => $this->mfo,
            'paylevel' => $this->paylevel,
            'phone' => $this->phone,
            'signer' => $this->signer,
            'account' => $this->account,
            'swift' => $this->swift,
            'website' => $this->website,
            'parent_id' => $this->parent_id,
        ];
    }
}
