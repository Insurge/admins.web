<?php
namespace App\Square\Exceptions;

use Illuminate\Queue\SerializesModels;

class Exception {

    use SerializesModels;

    public function __construct()
    {

    }
}