<?php
namespace App\Square;

use SquareConnect\Api\OrdersApi;
use SquareConnect\Configuration;
use SquareConnect\Api\CustomersApi;
use SquareConnect\Api\LocationsApi;
use SquareConnect\Api\TransactionsApi;

class SquareConfig {

    /**
     * @var \Illuminate\Config\Repository|array
     */
    protected $config;

    /**
     * @var \SquareConnect\Api\LocationsApi
     */
    public $locationsAPI;
    /**
     * @var \SquareConnect\Api\CustomersApi
     */
    public $customersAPI;
    /**
     * @var \SquareConnect\Api\TransactionsApi
     */
    public $transactionsAPI;
    /**
     * @var \SquareConnect\Api\OrdersApi
     */
    public $ordersAPI;

    /**
     * SquareConfig constructor.
     */
    public function __construct()
    {
        $this->config = config('square');
        $this->setAccessToken($this->config['square_token']);
        $this->locationsAPI = new LocationsApi();
        $this->customersAPI = new CustomersApi();
        $this->transactionsAPI = new TransactionsApi();
        $this->ordersAPI = new OrdersApi();
    }

    /**
     * Access token for square.
     *
     * @param string $accessToken
     *
     * @return void
     */
    public function setAccessToken(string $accessToken)
    {
        Configuration::getDefaultConfiguration()->setAccessToken($accessToken);
    }

    /**
     * Api for locations.
     *
     * @return \SquareConnect\Api\LocationsApi
     */
    public function locationsAPI()
    {
        return $this->locationsAPI;
    }

    /**
     * Api for customers.
     *
     * @return \SquareConnect\Api\CustomersApi
     */
    public function customersAPI()
    {
        return $this->customersAPI;
    }

    /**
     * Api for transactions.
     *
     * @return \SquareConnect\Api\TransactionsApi
     */
    public function transactionsAPI()
    {
        return $this->transactionsAPI;
    }

    /**
     * Api for orders.
     *
     * @return \SquareConnect\Api\ordersApi
     */
    public function ordersAPI()
    {
        return $this->ordersAPI;
    }

    /**
     * Getter for config.
     *
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }
}