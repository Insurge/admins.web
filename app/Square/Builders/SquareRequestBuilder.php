<?php
namespace App\Square\Builders;

use App\User;
use SquareConnect\Model\ChargeRequest;
use SquareConnect\Model\CreateCustomerCardRequest;

class SquareRequestBuilder {

    /**
     * @param array $prepData
     * @return ChargeRequest
     */
    public function buildChargeRequest(array $prepData)
    {
        return new ChargeRequest($prepData);
    }

    /**
     * @param User $user
     * @return CreateCustomerCardRequest
     */
    public function buildCustomerRequest(User $user)
    {
        $data = [
            'given_name'    => $user->firstName,
            'family_name'   => $user->lastName,
            'company_name'  => $user->companyTitle,
            'email_address' => $user->email,
            'phone_number'  => $user->cellphone
        ];

        return new CreateCustomerCardRequest($data);
    }
}