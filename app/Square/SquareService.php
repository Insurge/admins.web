<?php
namespace App\Square;

use App\Payment;
use App\Square\Builders\SquareRequestBuilder;
use App\Square\Utils\Constants;
use SquareConnect\ApiException;

class SquareService{

    /**
     * @var SquareConfig
     */
    public $config;

    /**
     * @var SquareRequestBuilder
     */
    private $squareBuilder;

    /**
     * SquareService constructor.
     * @param SquareConfig $squareConfig
     */
    public function __construct(SquareConfig $squareConfig)
    {
        $this->config = $squareConfig;
        $this->squareBuilder = new SquareRequestBuilder();
    }

    /**
     * @return \SquareConnect\Model\ListLocationsResponse
     * @throws \SquareConnect\ApiException
     */
    public function locations()
    {
        return $this->config->locationsAPI->listLocations();
    }

    public function charge(array $data, int $user_id)
    {
        $currency = array_key_exists('currency', $data) ? $data['currency'] : 'USD';
        $prepData = [
            'idempotency_key' => uniqid(),
            'amount_money'    => [
                'amount'   => $data['amount'],
                'currency' => $currency,
            ],
            'card_nonce' => $data['card_nonce'],
        ];

        $transaction = new Payment(['sum' => $data['amount'], 'user_id' => $user_id, 'status' => Constants::TRANSACTION_STATUS_OPENED]);
        $transaction->save();

        try {
            $chargeRequest = $this->squareBuilder->buildChargeRequest($prepData);
            $response = $this->config->transactionsAPI->charge($data['location_id'], $chargeRequest)->getTransaction();

            $transaction->payment_service_id = $response->getId();
            $transaction->status = Constants::TRANSACTION_STATUS_PASSED;
            $transaction->save();
        } catch (ApiException $exception) {

            $transaction->payment_service_id = null;
            $transaction->status = Constants::TRANSACTION_STATUS_FAILED;
            $transaction->save();

            throw $exception;
        }

        return $transaction;
    }
}