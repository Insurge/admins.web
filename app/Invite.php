<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Exception;

class Invite extends Model
{
    protected $fillable = [
        'partner_id', 'hash'
    ];

    public $timestamps = false;

    public function partner()
    {
        return $this->belongsTo('App\Partner');
    }

    public function setHashAttribute($value)
    {
        $str = str_random(12);
        $this->attributes['hash'] = hash_hmac('sha256', $str, 'secret');
    }

    public static function boot()
    {
        parent::boot();

        self::created(function ($model) {

            $partners_app = config('custom.partners_app');

            $path = [
                'path' => $partners_app . 'invite/' . $model->hash
            ];

            $seller = $model->partner;

            try {
                Mail::send('emails.invite', $path, function($message) use ($seller) {
                    $message->to($seller->login, $seller->login)
                        ->subject('Safor');
                    $message->from($seller->login,'Safor');
                });
            } catch (Exception $ex) {
                info('bad send message');
            }
        });
    }
}
