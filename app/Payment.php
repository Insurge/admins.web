<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'user_id', 'rate', 'nextpay', 'status', 'payment_service_id', 'sum'
    ];
}
