<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
  protected $fillable = [
    'partner_id', 'date', 'name', 'path', 'disabled'
  ];

  public function partner()
  {
      return $this->belongsTo(Partner::class);
  }
}
