<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ptype extends Model
{
  protected $fillable = [
    'kind', 'cashflow', 'percent', 'm1', 'm3', 'm6', 'm12', 'm24'
  ];

  public $timestamps = false;

  public function partners()
  {
    return $this->belongsToMany('App\Partner', 'type_id', 'id');
  }

  public function qrc()
  {
    return $this->belongsTo('App\Qrc');
  }
}
