<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'qrc',
        'fio',
        'photo',
        'status',
        'companyId',
        'companyTitle',
        'cellphone',
        'email',
        'emailVerified',
        'phoneVerified',
        'firstName',
        'middleName',
        'lastName',
        'gender',
        'birthDate',
        'region',
        'idPhoto',
        'selfiePhoto',
        'pay_hash'
    ];

    public function invoices()
    {
        return $this->hasMany('App\Invoice');
    }

    public function tickets()
    {
        return $this->hasMany('App\Ticket');
    }
  }
