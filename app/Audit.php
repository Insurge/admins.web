<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{

  protected $fillable = [
    'partner_id', 'date', 'action', 'object_type', 'object_id', 'ip'
  ];

  protected $table = 'audit';
  
}
