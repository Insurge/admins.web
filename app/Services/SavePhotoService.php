<?php

namespace App\Services;

use App\User;
use Illuminate\Http\Request;

class SavePhotoService {

    public function save(Request $request, User $user, array $files)
    {

        foreach ($files as $photo) {

            if($this->canHandleImage($photo)) {

                $file = $request->file($photo);
                $filename = time() . $photo . '.jpg';

                $file->move($this->getImageDirectory(), $filename);
                $user->$photo = $filename;
                $user->save();
            }
        }
    }

    protected function canHandleImage($file)
    {
        return request()->hasFile($file);
    }

    protected function getImageDirectory()
    {
        return public_path() . '/images/users';
    }
}