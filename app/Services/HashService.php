<?php

namespace App\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

/**
 * Class HashService
 * @package App\Services
 */
class HashService
{

    /**
     * Hash method for create hash code for qrc
     */
    protected $hash_method = 'sha256';

    /**
     * Prefix for generation hash code
     */
    protected $prefix = 'SAFOR-UA-';

    /**
     * @param string $salt
     * @return string
     */
    public function hash(string $salt): string {

        $timestamp = Carbon::now()->timestamp;

        $hash = Hash::make($this->prefix . $timestamp . $salt);
        $hash = hash_hmac($this->hash_method, $hash, $salt);

        return $hash;
    }


}