<?php 

namespace App\Services;

use App\Http\Resources\QrcResource;
use App\Log\QRC\AddQRC;
use App\Log\QRC\EditQRC;
use App\Partner;
use App\Ptype;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use QrCode;
use App\Qrc;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use DB;

class QRCService {
    
    /**
     * Hash method for create hash code for qrc
     */
    protected $hash_method = 'sha256';

    /**
     * Prefix for generation hash code
     */
    protected $prefix = 'SAFOR-UA-';

    protected $logService;
    protected $hashService;

    /**
     * QRCService constructor.
     * @param LogService $logService
     * @param HashService $hashService
     */
    public function __construct(LogService $logService, HashService $hashService)
    {
        $this->logService = $logService;
        $this->hashService = $hashService;
    }

    /**
     * @param Partner $partner
     * @param Ptype $ptype
     * @param array $qrcs
     * @return AnonymousResourceCollection
     * @throws \Exception
     */
    public function addQRCs(Partner $partner, PType $ptype, array $qrcs) {

        $count = 0;
        $newQrcs = [];
        foreach ($qrcs as $qrc) {

            $count++;

            $resultName = empty($qrc['name']) ? 'QRC для типа: ' . $ptype->kind : $qrc['name'];

            $hash = $this->hashService->hash($partner->salt);

            $qrc = Qrc::create([
                'partner_id' => $partner->id,
                'name' => $resultName,
                'qrc' => $hash,
                'ptype_id' => $ptype->id,
                'disabled' => $qrc['disabled']
            ]);

            $newQrcs[] = $qrc;

            $this->logService->addLog(new AddQRC($qrc->qrc));

        }

        return QrcResource::collection(collect($newQrcs));
    }


    /**
     * Update Qrc by id
     *
     * @param Integer $id
     * @param String $name
     * @param Boolean $disabled
     * @throws \Exception
     */
    public function updateQrc(int $id, string $name, bool $disabled, LogService $logService) {

        $qrcOld = Qrc::find($id);

        DB::table('qrcs')
            ->where('id', $id)
            ->update(compact('name', 'disabled'));

        $logService->addLog(new EditQRC($id, $name, $disabled, $qrcOld));

    }
}