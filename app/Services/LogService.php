<?php

namespace App\Services;


use App\Log;
use App\Log\LogFather;
use DateTime;
use JWTAuth;

/**
 * Class LogService
 * @package App\Services
 */
class LogService
{

    /**
     * @param LogFather $logFather
     * @return Log
     * @throws \Exception
     */
    public function addLog(LogFather $logFather) : bool {

        $user = JWTAuth::user();

        $date = new DateTime();
        if ($logFather->isSave()) {
            Log::create([
                'passwd_id' => $user->id,
                'doing' => $logFather->serializable(),
                'created_at' => $date->format('Y-m-d H:i:s')
            ]);
            return true;
        } else {
            return false;
        }
    }
}