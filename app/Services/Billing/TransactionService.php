<?php
namespace App\Services\Billing;

use Illuminate\Http\Request;

class TransactionService {

    public function test(Request $request)
    {
        $data = $request->all();
        $nonce = $data['nonce'];
        if (is_null($nonce)) {
            echo "Invalid card data";
            http_response_code(422);
            return;
        }
        \SquareConnect\Configuration::getDefaultConfiguration()->setAccessToken('sandbox-sq0atb-WwoZ_FVFjs5nO2RGLzzGTg');
        $transactions_api = new \SquareConnect\Api\TransactionsApi();

        $request_body = [
            "card_nonce" => $nonce,
            "amount_money" => [
                "amount" => 100,
                "currency" => "GBP"
            ],
            "buyer_email_address" => "email@emai.ru",
            "idempotency_key" => uniqid()
        ];

        $location_id = 'CBASEM92yB3AF-LXdF48T5fNHHcgAQ';

        try {
            $result = $transactions_api->charge($location_id, $request_body);
            echo "<pre>";
            print_r($result);
            echo "</pre>";
        } catch (\SquareConnect\ApiException $e) {
            echo "Caught exception!<br/>";
            print_r("<strong>Response body:</strong><br/>");
            echo "<pre>"; var_dump($e->getResponseBody()); echo "</pre>";
            echo "<br/><strong>Response headers:</strong><br/>";
            echo "<pre>"; var_dump($e->getResponseHeaders()); echo "</pre>";
        }
    }
}