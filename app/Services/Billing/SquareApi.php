<?php
namespace App\Services\Billing;

class SquareApi {

    /**
     * @var string
     */
    protected $square_app_id;

    /**
     * @var string
     */
    protected $square_token;

    /**
     * @var string
     */
    protected $square_location;

    /**
     * SquareApi constructor
     */
    public function __construct()
    {
        $this->square_app_id = config('square.square_application_id');
        $this->square_token = config('square.square_token');
        $this->square_location = config('square.square_location');
        $this->setAccessToken();
    }

    /**
     * @return \Illuminate\Config\Repository|mixed|string
     */
    public function getSquareAccessToken()
    {
        return $this->square_token;
    }

    /**
     * @return \Illuminate\Config\Repository|mixed|string
     */
    public function getSquareLocation()
    {
        return $this->square_location;
    }

    protected function setAccessToken()
    {
        \SquareConnect\Configuration::getDefaultConfiguration()->setAccessToken($this->getSquareAccessToken());
    }
}