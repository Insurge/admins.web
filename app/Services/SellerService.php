<?php

namespace App\Services;

use App\Log\Seller\AddSeller;
use App\Partner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\SellerResource;
use App\Invite;

class SellerService {


    protected $logService;

    protected $defaultRules = [
        'name' => 'required|string|max:255',
        'login' => 'required|string|email|max:255|unique:partners',
        'phone' => 'required|regex:/[0-9]{11}/'
    ];

    /**
     * SellerService constructor.
     * @param LogService $logService
     */
    public function __construct(LogService $logService)
    {
        $this->logService = $logService;
    }


    /**
     * @param Request $request
     * @param int $added_by
     * @param LogService $logService
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function create(Request $request, int $added_by)
    {
        $data = $request->all();

        $validator = $this->validate($data, $this->defaultRules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $data['password'] = Hash::make(str_random(12));
        $data['added_by'] = $added_by;
        $data['is_sale'] = 1;

        $seller = Partner::create($data);

        Invite::create([
            'partner_id' => $seller->id,
            'hash' => ''
        ]);

        $seller = new SellerResource($seller);

        $this->logService->addLog(new AddSeller($seller));

        return response()->json([
            'seller' => $seller
        ], 200);
    }

    /**
     * @param $data
     * @param array $rules
     * @return mixed
     */
    protected function validate($data, $rules = [])
    {
        return Validator::make($data, $rules);
    }
}