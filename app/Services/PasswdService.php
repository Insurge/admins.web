<?php

namespace App\Services;

use App\Http\Resources\PasswdForLog;
use App\Http\Resources\PasswdResource;
use App\Log\Admin\AddAdmin;
use App\Log\Admin\EditAdmin;
use App\Passwd;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;

class PasswdService {

    protected $logService;

    /**
     * @var array
     */
    protected $defaultRules = [
        'name'     => 'required|string|max:255',
        'login'    => 'required|string|email|max:255',
        'phone'    => 'required|regex:/[0-9]{11}/'
    ];

    /**
     * @param LogService $logService
     */
    public function __construct(LogService $logService)
    {
        $this->logService = $logService;
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function create(Request $request)
    {
        $data = $request->all();

        $rules = $this->defaultRules;
        $rules['password'] = 'required|string|min:6|confirmed';
        $rules['login'] = 'required|string|email|max:255|unique:passwds';

        $validator = $this->validation($data, $rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $data['password'] = Hash::make($request->get('password'));

        $passwd = Passwd::create($data);
        $token = JWTAuth::fromUser($passwd);

        $this->logService->addLog(new AddAdmin($passwd->id));

        return compact('passwd', 'token');
    }

    /**
     * @param $data
     * @return mixed
     */
    protected function validation($data, $rules = [])
    {
        $validator = Validator::make($data, $rules);

        return $validator;
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $rules = $this->defaultRules;
        $validator = $this->validation($data, $rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $params = $this->checkPass($data);

        $user = Passwd::findOrFail($id);
        $oldValues = new PasswdForLog($user->replicate());

        $user->update($params);
        $resultUser = new PasswdForLog($user);
        $this->logService->addLog(new EditAdmin($oldValues, $resultUser, $resultUser->id));

        return response()->json([
            "user" => $resultUser
        ], 200);
    }

    /**
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    protected function checkPass($data)
    {
        if (isset($data['password'])) {

            $validator = Validator::make($data, [
                'password' => 'required|string|min:6|confirmed'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }

            $data['password'] = Hash::make($data['password']);
        }

        return $data;
    }
}