<?php

namespace App\Services;


use App\Jobs\ProcessUser;
use App\Ticket;
use Illuminate\Bus\Dispatcher;
use Symfony\Component\HttpFoundation\Session\Session;
use Illuminate\Support\Facades\DB;

class TicketService
{

    private $prefixTicket = 'ticket_';
    private $queueName = 'tickets';

    /**
     * @param int $idTicket
     * @param string $action
     */
    public function resolve(int $idTicket) {

        $this->reject($idTicket, 'resolve');
    }

    /**
     * @param int $ticketId
     * @param string $action
     * @param string|null $reason
     */
    public function reject(int $ticketId, string $action = 'reject', string $reason = null) {

        $processUser = new ProcessUser(compact('ticketId', 'action', 'reason'));
        $processUser->onQueue($this->queueName)->delay(now()->addMinute(5));
        $jobId = app(Dispatcher::class)->dispatch($processUser);

        $session = new Session();
        $session->set($this->prefixTicket . $ticketId, $jobId);
    }


    /**
     * @param int $ticketId
     */
    public function cancel(int $ticketId) {

        $session = new Session();
        $jobId = $session->get($this->prefixTicket . $ticketId);
        $session->remove($this->prefixTicket .$ticketId);

        DB::table('jobs')->where('id', '=', $jobId)->delete();

    }

    /**
     * @param int $ticketId
     * @return bool
     */
    public function checkIfExist(int $ticketId) : bool {
        return Ticket::where('id', $ticketId)->exists();
    }

}