<?php

namespace App\Providers;

use App\Services\Billing\SquareApi;
use App\Services\HashService;
use App\Services\LogService;
use App\Services\PasswdService;
use App\Services\SavePhotoService;
use App\Services\TicketService;
use App\Square\SquareConfig;
use App\Square\SquareService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Helpers\PartnerHelper;
use App\Services\QRCService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(PartnerHelper::class, function ($app) {
            return new PartnerHelper();
        });

        $this->app->singleton(LogService::class, function ($app) {
            return new LogService();
        });

        $this->app->singleton(HashService::class, function ($app) {
            return new HashService();
        });

        $this->app->singleton(SavePhotoService::class, function ($app) {
            return new SavePhotoService();
        });

        $this->app->singleton(PasswdService::class, function ($app) {
            return new PasswdService($app->make(LogService::class));
        });

        $this->app->singleton(QRCService::class, function ($app) {
            return new QRCService($app->make(LogService::class), $app->make(HashService::class));
        });

        $this->app->singleton(TicketService::class, function ($app) {
            return new TicketService();
        });

        /*$this->app->singleton(SquareApi::class, function ($app) {
            return new SquareApi();
        });*/
        $this->app->singleton(SquareConfig::class, function ($app) {
            return new SquareConfig();
        });

        $this->app->bind(SquareService::class);

        $this->app->alias(SquareService::class, 'square');
    }
}
