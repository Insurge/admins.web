<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'start_date', 'end_date', 'user_id', 'status', 'type'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
