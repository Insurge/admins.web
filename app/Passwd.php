<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Passwd extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'login', 'password', 'phone', 'disabled', 'super', 'addpartner', 'editpartner', 'blockpartner', 'adddocs', 'viewbalance', 'acceptbills', 'viewbills', 'disabledocs', 'logaccess', 'is_moderator'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    /**
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


    public function partners()
    {
        return $this->belongsToMany('App\Partner', 'added_by', 'id');
    }

    public function logs()
    {
        return $this->hasMany( 'App\Log');
    }

    public function getFieldForLog()
    {
        return [
            'name' => $this->name,
            'phone' => $this->phone,
            'disabled' => $this->disabled,
            'super' => $this->super,
            'addpartner' => $this->addpartner,
            'editpartner' => $this->editpartner,
            'blockpartner' => $this->blockpartner,
            'adddocs' => $this->adddocs,
            'viewbalance' => $this->viewbalance,
            'acceptbills' => $this->acceptbills,
            'viewbills' => $this->viewbills,
            'disabledocs' => $this->disabledocs,
            'logaccess' => $this->logaccess,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
