<?php

namespace App\Log\QRC;


use App\Log\LogFather;
use App\Qrc;

class EditQRC implements LogFather
{

    protected $modelName = 'QRC';
    protected $actionName = 'Edit';
    protected $id = '';
    protected $hash = '';
    protected $oldValues = [];
    protected $save = true;

    /**
     * EditQRC constructor.
     * @param int $id
     * @param string $name
     * @param bool $disabled
     * @param Qrc $qrcOld
     */
    public function __construct(int $id, string $name, bool $disabled, Qrc $qrcOld)
    {
        if ($qrcOld->name === $name && !!$qrcOld->disabled === $disabled) {
            $this->save = false;
            return;
        }
        if ($qrcOld->name !== $name) {
            $this->oldValues['name'] = $qrcOld->name;
        }
        if (!!$qrcOld->disabled !== $disabled) {
            $this->oldValues['disabled'] = $qrcOld->disabled;
        }
        $this->id = $id;
        $this->hash = $qrcOld->qrc;
        $this->save = true;
    }


    /**
     * Method get object for save
     *
     * @return array
     */
    public function getSavedObject(): array
    {
        return [
            'modelName' => $this->modelName,
            'actionName' => $this->actionName,
            'id' => $this->id,
            'hash' => $this->hash,
            'oldValues' => $this->oldValues
        ];
    }

    /**
     * Method serializable data the object for save
     *
     * @return string
     */
    public function serializable(): string
    {
        return json_encode($this->getSavedObject());
    }

    /**
     * @return bool
     */
    public function isSave(): bool
    {
        return $this->save;
    }
}