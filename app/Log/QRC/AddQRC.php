<?php

namespace App\Log\QRC;

use App\Log\LogFather;

/**
 * Class AddQRC
 * @package App\Log\QRC
 */
class AddQRC implements LogFather
{
    protected $modelName = 'QRC';
    protected $actionName = 'Add';
    protected $hash = '';

    /**
     * AddQRC constructor.
     * @param string $hash
     */
    public function __construct(string $hash)
    {
        $this->hash = $hash;
    }


    /**
     * Method get object for save
     *
     * @return array
     */
    public function getSavedObject(): array
    {
        return [
            'modelName' => $this->modelName,
            'actionName' => $this->actionName,
            'hash' => $this->hash
        ];
    }

    /**
     * Method serializable data the object for save
     *
     * @return string
     */
    public function serializable(): string
    {
        return json_encode($this->getSavedObject());
    }

    /**
     * @return bool
     */
    public function isSave(): bool
    {
        return true;
    }
}