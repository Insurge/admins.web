<?php

namespace App\Log\Seller;


use App\Http\Resources\SellerResource;
use App\Log\LogFather;

class AddSeller implements LogFather
{

    protected $modelName = 'Seller';
    protected $actionName = 'Add';
    protected $newValues = [];

    /**
     * AddSeller constructor.
     */
    public function __construct(SellerResource $sellerResource)
    {
        $this->newValues = $sellerResource;
    }

    /**
     * Method get object for save
     *
     * @return array
     */
    public function getSavedObject(): array
    {
        return [
            'modelName' => $this->modelName,
            'actionName' => $this->actionName,
            'id' => $this->newValues->id
        ];
    }

    /**
     * Method serializable data the object for save
     *
     * @return string
     */
    public function serializable(): string
    {
        return json_encode($this->getSavedObject());
    }

    /**
     * @return bool
     */
    public function isSave(): bool
    {
        return true;
    }
}