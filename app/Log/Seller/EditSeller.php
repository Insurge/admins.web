<?php

namespace App\Log\Seller;


use App\Http\Resources\SellerResource;
use App\Log\LogFather;

class EditSeller implements LogFather
{

    protected $modelName = 'Seller';
    protected $actionName = 'Edit';
    protected $oldValues = [];
    protected $save = false;
    protected $id = '';

    /**
     * EditSeller constructor.
     * @param SellerResource $oldValues
     * @param SellerResource $newValues
     */
    public function __construct(SellerResource $oldValues, SellerResource $newValues, $id)
    {
        $namesEquals = $oldValues->name === $newValues->name;
        $phoneEquals = $oldValues->phone === $newValues->phone;
        $disabledEquals = $oldValues->disabled === $newValues->disabled;
        $this->id = $id;
        if (!$namesEquals ) {
            $this->oldValues['name'] = $oldValues->name;
        }

        if (!$phoneEquals) {
            $this->oldValues['phone'] = $oldValues->phone;
        }

        if (!$disabledEquals) {
            $this->oldValues['disabled'] = $oldValues->disabled ;
        }
        if (!empty($this->oldValues)) {
            $this->save =    true;
        }
    }


    /**
     * Method get object for save
     *
     * @return array
     */
    public function getSavedObject(): array
    {
        return [
            'id' => $this->id,
            'modelName' => $this->modelName,
            'actionName' => $this->actionName,
            'oldValues' => $this->oldValues
        ];
    }

    /**
     * Method serializable data the object for save
     *
     * @return string
     */
    public function serializable(): string
    {
        return json_encode($this->getSavedObject());
    }

    /**
     * @return bool
     */
    public function isSave(): bool
    {
        return $this->save;
    }
}