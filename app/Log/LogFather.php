<?php

namespace App\Log;


interface LogFather
{

    /**
     * Method get object for save
     *
     * @return array
     */
    public function getSavedObject() : array;

    /**
     * Method serializable data the object for save
     *
     * @return string
     */
    public function serializable() : string;


    /**
     * @return bool
     */
    public function isSave() : bool;
}