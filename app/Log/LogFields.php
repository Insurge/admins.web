<?php

namespace App\Log;


/**
 * Interface LogFields
 * @package App\Log
 */
interface LogFields
{
    /**
     * @return array
     */
    public function getFieldForLog() : array;

}