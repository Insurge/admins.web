<?php

namespace App\Log\Document;

use App\Http\Resources\DocumentResource;
use App\Log\LogFather;

class AddDocument implements LogFather
{

    protected $modelName = 'Document';
    protected $actionName = 'Add';
    protected $newValues = [];

    /**
     * AddDocument constructor.
     * @param DocumentResource $document
     */
    public function __construct(DocumentResource $document)
    {
        $this->newValues = $document;
    }


    /**
     * Method get object for save
     *
     * @return array
     */
    public function getSavedObject(): array
    {
        return [
            'modelName' => $this->modelName,
            'actionName' => $this->actionName,
            'newValues' => [
                'id' => $this->newValues->id,
                'name' => $this->newValues->name,
                'path' => $this->newValues->path,
                'date' => $this->newValues->date
            ]
        ];
    }

    /**
     * Method serializable data the object for save
     *
     * @return string
     */
    public function serializable(): string
    {
        return json_encode($this->getSavedObject());
    }

    /**
     * @return bool
     */
    public function isSave(): bool
    {
        return true;
    }
}