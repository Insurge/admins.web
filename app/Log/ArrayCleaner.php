<?php

namespace App\Log;


/**
 * Class ArrayCleaner
 * Clean array from empty fields
 *
 * @package App\Log
 */
class ArrayCleaner
{
    /**
     * @param array $arr
     * @return array
     */
    public function clear(array $arr) : array {
        $result = [];
        foreach ($arr as $key => $value){
            if (isset($value) && !empty($value)) {
                $result[$key] = $value;
            }
        }
        return $result;
    }
}