<?php

namespace App\Log\Partner;

use App\Log\LogFather;
use App\Log\LogFields;
use Symfony\Component\HttpFoundation\Request;

class EditPartner implements LogFather
{

    protected $modelName = 'Partner';
    protected $actionName = 'Edit';
    protected $id = '';
    protected $oldValues = [];
    protected $newValues = [];
    protected $save = true;

    /**
     * EditPartner constructor.
     * @param LogFields $partner
     * @param Request $request
     */
    public function __construct(LogFields $partner, Request $request)
    {
        $this->save = false;
        foreach ($partner->getFieldForLog() as $key => $oldValue) {
            $newValue = $request->$key;

            if ($oldValue !== $newValue) {
                $this->oldValues[$key] = $oldValue;
                $this->newValues[$key] = $newValue;
                $this->save = true;
            }
        }
        $this->id = $partner->id;
    }

    /**
     * Method get object for save
     *
     * @return array
     */
    public function getSavedObject(): array
    {
        return [
            'modelName' => $this->modelName,
            'actionName' => $this->actionName,
            'oldValues' => $this->oldValues,
            'id' => $this->id
        ];
    }

    /**
     * Method serializable data the object for save
     *
     * @return string
     */
    public function serializable(): string
    {
        return json_encode($this->getSavedObject());
    }

    /**
     * @return bool
     */
    public function isSave(): bool
    {
        return $this->save;
    }
}