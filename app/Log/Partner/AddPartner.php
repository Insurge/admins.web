<?php

namespace App\Log\Partner;

use App\Log\LogFather;

class AddPartner implements LogFather
{

    protected $modelName = 'Partner';
    protected $actionName = 'Add';
    protected $id = '';
    protected $save = true;

    /**
     * AddPartner constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }


    /**
     * Method get object for save
     *
     * @return array
     */
    public function getSavedObject(): array
    {
        return [
          'modelName' => $this->modelName,
          'actionName' => $this->actionName,
          'id' => $this->id
        ];
    }

    /**
     * Method serializable data the object for save
     *
     * @return string
     */
    public function serializable(): string
    {
        return json_encode($this->getSavedObject());
    }

    /**
     * @return bool
     */
    public function isSave(): bool
    {
        return true;
    }
}