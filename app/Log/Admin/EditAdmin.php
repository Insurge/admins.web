<?php

namespace App\Log\Admin;

use App\Http\Resources\PasswdForLog;
use App\Log\LogFather;
use App\Services\PasswdService;

class EditAdmin implements LogFather
{

    /**
     * @var string
     */
    protected $modelName = 'Admin';

    /**
     * @var string
     */
    protected $actionName = 'Edit';

    /**
     * @var array
     */
    protected $oldValues = [];

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var bool
     */
    protected $save = false;

    /**
     * EditAdmin constructor.
     * @param $oldValues
     * @param $newValues
     * @param $id
     */
    public function __construct(PasswdForLog $oldValues, PasswdForLog $newValues, $id)
    {
        $differenceFields = $oldValues->getDifferentFields($newValues);
        if (!empty($differenceFields)) {
            $this->save = true;
        }
        $this->id = $id;

        $this->oldValues = $differenceFields;

    }

    /**
     * Method get object for save
     *
     * @return array
     */
    public function getSavedObject(): array
    {
        return [
            'modelName' => $this->modelName,
            'actionName' => $this->actionName,
            'oldValues' => $this->oldValues,
            'id' => $this->id
        ];
    }

    /**
     * Method serializable data the object for save
     *
     * @return string
     */
    public function serializable(): string
    {
        return json_encode($this->getSavedObject());
    }

    /**
     * @return bool
     */
    public function isSave(): bool
    {
        return $this->save;
    }
}