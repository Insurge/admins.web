<?php

namespace App\Jobs;

use App\Ticket;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Symfony\Component\HttpFoundation\Session\Session;

class ProcessUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $ticketId;
    protected $action;
    protected $reasons;

    /**
     * Create a new job instance.
     *
     * @param array $processInfo
     *      @key int $userId
     *      @key int status
     */
    public function __construct(array $processInfo)
    {
        $this->ticketId = $processInfo['ticketId'];
        $this->action = $processInfo['action'];
        $this->reasons = $processInfo['reason'] || '';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->action === 'resolve') {
            $this->resolve();
        } else {
            $this->reject();
        }
        $this->removeFromSession();

    }

    public function resolve() {

        $ticket = Ticket::find($this->ticketId);
        $user = $ticket->user;
        $user->status = true;
        $user->save();
        $ticket->delete();
    }

    public function reject() {

        $ticket = Ticket::find($this->ticketId);
        $user = $ticket->user;
        $ticket->delete();

        info('User with id: ' . $user->id . 'unverified');
    }

    public function removeFromSession() {
        $session = new Session();
        $session->remove('ticket_' . $this->ticketId);
    }
}
