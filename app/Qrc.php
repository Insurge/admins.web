<?php

namespace App;

use App\Log\LogFields;
use Illuminate\Database\Eloquent\Model;

class Qrc extends Model implements LogFields
{
  protected $fillable = [
    'partner_id', 'qrc', 'ptype_id', 'disabled', 'name'
  ];

  public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
  public function partner()
  {
    return $this->belongsTo('App\Partner');
  }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
  public function ptype()
  {
    return $this->belongsTo('App\Ptype');
  }

  /**
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function payments() {
      return $this->hasMany('App\QrcPayments');
  }

  public static function UnlinkImage($filepath)
  {
    $old_image = '/public' . $filepath;
    if (file_exists($old_image)) {
      @unlink($old_image);
    }
  }

    /**
     * @return array
     */
    public function getFieldForLog(): array
    {
        return [
            'qrc' => $this->qrc,
            'pType' => $this->ptype->kind,
            'disabled' => $this->disabled,
            'name' => $this->name,
            'partner_id' => $this->partner_id
        ];
    }
}
